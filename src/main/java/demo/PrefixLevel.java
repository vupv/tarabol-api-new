package demo;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;


/**
 * The persistent class for the PREFIX_LEVEL database table.
 * 
 */
@Entity
@Table(name="PREFIX_LEVEL")
@NamedQuery(name="PrefixLevel.findAll", query="SELECT p FROM PrefixLevel p")
public class PrefixLevel implements Serializable {
	private static final long serialVersionUID = 1L;
	private long levelid;
	private String levelname;
	private List<PrefixCard> prefixCards;
	private PrefixCours prefixCours;

	public PrefixLevel() {
	}


	@Id
	@Column(name="LEVELID", unique=true, nullable=false)
	public long getLevelid() {
		return this.levelid;
	}

	public void setLevelid(long levelid) {
		this.levelid = levelid;
	}


	@Column(name="LEVELNAME", length=1000)
	public String getLevelname() {
		return this.levelname;
	}

	public void setLevelname(String levelname) {
		this.levelname = levelname;
	}


	//bi-directional many-to-one association to PrefixCard
	@JsonManagedReference(value="card-level")
	@OneToMany(mappedBy="prefixLevel", fetch=FetchType.EAGER)
	public List<PrefixCard> getPrefixCards() {
		return this.prefixCards;
	}

	public void setPrefixCards(List<PrefixCard> prefixCards) {
		this.prefixCards = prefixCards;
	}

	public PrefixCard addPrefixCard(PrefixCard prefixCard) {
		getPrefixCards().add(prefixCard);
		prefixCard.setPrefixLevel(this);

		return prefixCard;
	}

	public PrefixCard removePrefixCard(PrefixCard prefixCard) {
		getPrefixCards().remove(prefixCard);
		prefixCard.setPrefixLevel(null);

		return prefixCard;
	}


	//bi-directional many-to-one association to PrefixCours
	@JsonBackReference(value="level-cours")
	@ManyToOne
	@JoinColumn(name="COURSESID")
	public PrefixCours getPrefixCours() {
		return this.prefixCours;
	}

	public void setPrefixCours(PrefixCours prefixCours) {
		this.prefixCours = prefixCours;
	}

}