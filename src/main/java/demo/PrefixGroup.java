package demo;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;


/**
 * The persistent class for the PREFIX_GROUP database table.
 * 
 */
@Entity
@Table(name="PREFIX_GROUP")
@NamedQuery(name="PrefixGroup.findAll", query="SELECT p FROM PrefixGroup p")
public class PrefixGroup implements Serializable {
	private static final long serialVersionUID = 1L;
	private long groupid;
	private String groupname;
	private List<PrefixGroupUser> prefixGroupUsers;

	public PrefixGroup() {
	}


	@Id
	@Column(name="GROUPID", unique=true, nullable=false)
	public long getGroupid() {
		return this.groupid;
	}

	public void setGroupid(long groupid) {
		this.groupid = groupid;
	}


	@Column(name="GROUPNAME", length=2000)
	public String getGroupname() {
		return this.groupname;
	}

	public void setGroupname(String groupname) {
		this.groupname = groupname;
	}


	//bi-directional many-to-one association to PrefixGroupUser
	@JsonManagedReference(value="groupuser-group")
	@OneToMany(mappedBy="prefixGroup", fetch=FetchType.EAGER)
	public List<PrefixGroupUser> getPrefixGroupUsers() {
		return this.prefixGroupUsers;
	}

	public void setPrefixGroupUsers(List<PrefixGroupUser> prefixGroupUsers) {
		this.prefixGroupUsers = prefixGroupUsers;
	}

	public PrefixGroupUser addPrefixGroupUser(PrefixGroupUser prefixGroupUser) {
		getPrefixGroupUsers().add(prefixGroupUser);
		prefixGroupUser.setPrefixGroup(this);

		return prefixGroupUser;
	}

	public PrefixGroupUser removePrefixGroupUser(PrefixGroupUser prefixGroupUser) {
		getPrefixGroupUsers().remove(prefixGroupUser);
		prefixGroupUser.setPrefixGroup(null);

		return prefixGroupUser;
	}

}