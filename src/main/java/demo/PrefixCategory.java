package demo;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;


/**
 * The persistent class for the PREFIX_CATEGORIES database table.
 * 
 */
@Entity
@Table(name="PREFIX_CATEGORIES")
@NamedQueries({
	@NamedQuery(name="PrefixCategory.findAll", query="SELECT p FROM PrefixCategory p"),
//	@NamedQuery(name="PrefixCategory.add", query="INSERT INTO PREFIX_CATEGORIES (CATEGORIESID, CATEGORIESNAME, PARENTID) VALUES (:categoriesid,  :categoriesname,  :parentid)"),
//	@NamedQuery(name="PrefixCategory.edit", query="UPDATE PREFIX_CATEGORIES cate SET cate.CATEGORIESNAME = :categoriesname, cate.PARENTID = :parentid WHERE CATEGORIESID = :categoriesid"),
	@NamedQuery(name="PrefixCategory.delete", query="DELETE FROM PrefixCategory p WHERE p.categoriesid = categoriesid"),
})
public class PrefixCategory implements Serializable {
	private static final long serialVersionUID = 1L;
	private long categoriesid;
	private String categoriesname;
	private PrefixCategory prefixCategory;
	private List<PrefixCategory> prefixCategories;

	public PrefixCategory() {
	}


	@Id
	@Column(name="CATEGORIESID", unique=true, nullable=false)
	public long getCategoriesid() {
		return this.categoriesid;
	}

	public void setCategoriesid(long categoriesid) {
		this.categoriesid = categoriesid;
	}


	@Column(name="CATEGORIESNAME", length=200)
	public String getCategoriesname() {
		return this.categoriesname;
	}

	public void setCategoriesname(String categoriesname) {
		this.categoriesname = categoriesname;
	}


	//bi-directional many-to-one association to PrefixCategory
	@JsonBackReference
	@ManyToOne
	@JoinColumn(name="PARENTID")
	public PrefixCategory getPrefixCategory() {
		return this.prefixCategory;
	}

	public void setPrefixCategory(PrefixCategory prefixCategory) {
		this.prefixCategory = prefixCategory;
	}


	//bi-directional many-to-one association to PrefixCategory
	@JsonManagedReference
	@OneToMany(mappedBy="prefixCategory", fetch=FetchType.EAGER)
	public List<PrefixCategory> getPrefixCategories() {
		return this.prefixCategories;
	}

	public void setPrefixCategories(List<PrefixCategory> prefixCategories) {
		this.prefixCategories = prefixCategories;
	}

	public PrefixCategory addPrefixCategory(PrefixCategory prefixCategory) {
		getPrefixCategories().add(prefixCategory);
		prefixCategory.setPrefixCategory(this);

		return prefixCategory;
	}

	public PrefixCategory removePrefixCategory(PrefixCategory prefixCategory) {
		getPrefixCategories().remove(prefixCategory);
		prefixCategory.setPrefixCategory(null);

		return prefixCategory;
	}

}