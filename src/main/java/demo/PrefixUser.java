package demo;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;


/**
 * The persistent class for the PREFIX_USER database table.
 * 
 */
@Entity
@Table(name="PREFIX_USER")
@NamedQueries(value = {
@NamedQuery(name="PrefixUser.findAll", query="SELECT p FROM PrefixUser p"),
@NamedQuery(name="PrefixUser.findByID", query="SELECT p FROM PrefixUser p WHERE p.userid = :userid"),
})
public class PrefixUser implements Serializable {
	private static final long serialVersionUID = 1L;
	private long userid;
	private String account;
	private String avatar;
	private String bio;
	private String email;
	private String language;
	private String password;
	private String timezone;
	private String username;
	private List<PrefixCoursesUser> prefixCoursesUsers;
	private List<PrefixFollowlist> prefixFollower;
	private List<PrefixFollowlist> prefixFollowlistsFollowing;
	private List<PrefixGroupUser> prefixGroupUsers;
	private PrefixLearnconfig prefixLearnconfig;
	private List<PrefixUserCardStatus> prefixUserCardStatuses;

	public PrefixUser() {
	}


	@Id
	@Column(name="USERID", unique=true, nullable=false)
	public long getUserid() {
		return this.userid;
	}

	public void setUserid(long userid) {
		this.userid = userid;
	}


	@Column(name="ACCOUNT", length=50)
	public String getAccount() {
		return this.account;
	}

	public void setAccount(String account) {
		this.account = account;
	}


	@Column(name="AVATAR", length=1000)
	public String getAvatar() {
		return this.avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}


	@Lob @Basic(fetch=FetchType.LAZY)
	@Column(name="BIO")
	public String getBio() {
		return this.bio;
	}

	public void setBio(String bio) {
		this.bio = bio;
	}


	@Column(name="EMAIL", length=300)
	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}


	@Column(name="\"LANGUAGE\"", length=100)
	public String getLanguage() {
		return this.language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}


	@Column(name="PASSWORD", nullable=false, length=50)
	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}


	@Column(name="TIMEZONE", length=100)
	public String getTimezone() {
		return this.timezone;
	}

	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}


	@Column(name="USERNAME", length=100)
	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}


	//bi-directional many-to-one association to PrefixCoursesUser
	@JsonManagedReference(value="coursesuser-user")
	@OneToMany(mappedBy="prefixUser", fetch=FetchType.EAGER)
	public List<PrefixCoursesUser> getPrefixCoursesUsers() {
		return this.prefixCoursesUsers;
	}

	public void setPrefixCoursesUsers(List<PrefixCoursesUser> prefixCoursesUsers) {
		this.prefixCoursesUsers = prefixCoursesUsers;
	}

	public PrefixCoursesUser addPrefixCoursesUser(PrefixCoursesUser prefixCoursesUser) {
		getPrefixCoursesUsers().add(prefixCoursesUser);
		prefixCoursesUser.setPrefixUser(this);

		return prefixCoursesUser;
	}

	public PrefixCoursesUser removePrefixCoursesUser(PrefixCoursesUser prefixCoursesUser) {
		getPrefixCoursesUsers().remove(prefixCoursesUser);
		prefixCoursesUser.setPrefixUser(null);

		return prefixCoursesUser;
	}


	//bi-directional many-to-one association to PrefixFollowlist
	@JsonManagedReference(value="follower-user")
	@OneToMany(mappedBy="prefixUserFollower", fetch=FetchType.EAGER)
	public List<PrefixFollowlist> getPrefixFollower() {
		return this.prefixFollower;
	}

	public void setPrefixFollower(List<PrefixFollowlist> prefixFollower) {
		this.prefixFollower = prefixFollower;
	}

	public PrefixFollowlist addPrefixFollower(PrefixFollowlist prefixFollower) {
		getPrefixFollower().add(prefixFollower);
		prefixFollower.setPrefixUserFollower(this);

		return prefixFollower;
	}

	public PrefixFollowlist removePrefixFollower(PrefixFollowlist prefixFollower) {
		getPrefixFollower().remove(prefixFollower);
		prefixFollower.setPrefixUserFollower(null);

		return prefixFollower;
	}


	//bi-directional many-to-one association to PrefixFollowlist
	@JsonManagedReference(value="fllowing-user")
	@OneToMany(mappedBy="prefixUserFollowing", fetch=FetchType.EAGER)
	public List<PrefixFollowlist> getPrefixFollowlistsFollowing() {
		return this.prefixFollowlistsFollowing;
	}

	public void setPrefixFollowlistsFollowing(List<PrefixFollowlist> prefixFollowlistsFollowing) {
		this.prefixFollowlistsFollowing = prefixFollowlistsFollowing;
	}

	public PrefixFollowlist addPrefixFollowlistsFollowing(PrefixFollowlist prefixFollowlistsFollowing) {
		getPrefixFollowlistsFollowing().add(prefixFollowlistsFollowing);
		prefixFollowlistsFollowing.setPrefixUserFollowing(this);

		return prefixFollowlistsFollowing;
	}

	public PrefixFollowlist removePrefixFollowlistsFollowing(PrefixFollowlist prefixFollowlistsFollowing) {
		getPrefixFollowlistsFollowing().remove(prefixFollowlistsFollowing);
		prefixFollowlistsFollowing.setPrefixUserFollowing(null);

		return prefixFollowlistsFollowing;
	}


	//bi-directional many-to-one association to PrefixGroupUser
	@JsonManagedReference(value="groupuser-user")
	@OneToMany(mappedBy="prefixUser", fetch=FetchType.EAGER)
	public List<PrefixGroupUser> getPrefixGroupUsers() {
		return this.prefixGroupUsers;
	}

	public void setPrefixGroupUsers(List<PrefixGroupUser> prefixGroupUsers) {
		this.prefixGroupUsers = prefixGroupUsers;
	}

	public PrefixGroupUser addPrefixGroupUser(PrefixGroupUser prefixGroupUser) {
		getPrefixGroupUsers().add(prefixGroupUser);
		prefixGroupUser.setPrefixUser(this);

		return prefixGroupUser;
	}

	public PrefixGroupUser removePrefixGroupUser(PrefixGroupUser prefixGroupUser) {
		getPrefixGroupUsers().remove(prefixGroupUser);
		prefixGroupUser.setPrefixUser(null);

		return prefixGroupUser;
	}


	//bi-directional many-to-one association to PrefixLearnconfig
	@JsonBackReference(value="user-learnconfig")
	@ManyToOne
	@JoinColumn(name="LEARNCONFIGID")
	public PrefixLearnconfig getPrefixLearnconfig() {
		return this.prefixLearnconfig;
	}

	public void setPrefixLearnconfig(PrefixLearnconfig prefixLearnconfig) {
		this.prefixLearnconfig = prefixLearnconfig;
	}


	//bi-directional many-to-one association to PrefixUserCardStatus
	@JsonManagedReference(value="usercardstatus-user")
	@OneToMany(mappedBy="prefixUser", fetch=FetchType.EAGER)
	public List<PrefixUserCardStatus> getPrefixUserCardStatuses() {
		return this.prefixUserCardStatuses;
	}

	public void setPrefixUserCardStatuses(List<PrefixUserCardStatus> prefixUserCardStatuses) {
		this.prefixUserCardStatuses = prefixUserCardStatuses;
	}

	public PrefixUserCardStatus addPrefixUserCardStatus(PrefixUserCardStatus prefixUserCardStatus) {
		getPrefixUserCardStatuses().add(prefixUserCardStatus);
		prefixUserCardStatus.setPrefixUser(this);

		return prefixUserCardStatus;
	}

	public PrefixUserCardStatus removePrefixUserCardStatus(PrefixUserCardStatus prefixUserCardStatus) {
		getPrefixUserCardStatuses().remove(prefixUserCardStatus);
		prefixUserCardStatus.setPrefixUser(null);

		return prefixUserCardStatus;
	}

}