package demo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;


/**
 * The persistent class for the PREFIX_FOLLOWLIST database table.
 * 
 */
@Entity
@Table(name="PREFIX_FOLLOWLIST")
@NamedQuery(name="PrefixFollowlist.findAll", query="SELECT p FROM PrefixFollowlist p")
public class PrefixFollowlist implements Serializable {
	private static final long serialVersionUID = 1L;
	private long followid;
	private PrefixUser prefixUserFollower;
	private PrefixUser prefixUserFollowing;

	public PrefixFollowlist() {
	}


	@Id
	@Column(name="FOLLOWID", unique=true, nullable=false)
	public long getFollowid() {
		return this.followid;
	}

	public void setFollowid(long followid) {
		this.followid = followid;
	}


	//bi-directional many-to-one association to PrefixUser
	@JsonBackReference(value="follower-user")
	@ManyToOne
	@JoinColumn(name="FOLLOWER")
	public PrefixUser getPrefixUserFollower() {
		return this.prefixUserFollower;
	}

	public void setPrefixUserFollower(PrefixUser prefixUserFollower) {
		this.prefixUserFollower = prefixUserFollower;
	}


	//bi-directional many-to-one association to PrefixUser
	@JsonBackReference(value="fllowing-user")
	@ManyToOne
	@JoinColumn(name="FOLLOWING")
	public PrefixUser getPrefixUserFollowing() {
		return this.prefixUserFollowing;
	}

	public void setPrefixUserFollowing(PrefixUser prefixUserFollowing) {
		this.prefixUserFollowing = prefixUserFollowing;
	}

}