package demo;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;


/**
 * The persistent class for the PREFIX_CARD database table.
 * 
 */
@Entity
@Table(name="PREFIX_CARD")
@NamedQuery(name="PrefixCard.findAll", query="SELECT p FROM PrefixCard p")
public class PrefixCard implements Serializable {
	private static final long serialVersionUID = 1L;
	private long cardid;
	private String audio;
	private String meaning;
	private String options;
	private String words;
	private PrefixLevel prefixLevel;
	private List<PrefixUserCardStatus> prefixUserCardStatuses;
	
	
	public PrefixCard() {
	}


	@Id
	@Column(name="CARDID", unique=true, nullable=false)
	public long getCardid() {
		return this.cardid;
	}

	public void setCardid(long cardid) {
		this.cardid = cardid;
	}


	@Column(name="AUDIO", length=2000)
	public String getAudio() {
		return this.audio;
	}

	public void setAudio(String audio) {
		this.audio = audio;
	}


	@Column(name="MEANING", length=2000)
	public String getMeaning() {
		return this.meaning;
	}

	public void setMeaning(String meaning) {
		this.meaning = meaning;
	}


	@Column(name="OPTIONS", length=200)
	public String getOptions() {
		return this.options;
	}

	public void setOptions(String options) {
		this.options = options;
	}


	@Column(name="WORDS", length=2000)
	public String getWords() {
		return this.words;
	}

	public void setWords(String words) {
		this.words = words;
	}


	//bi-directional many-to-one association to PrefixLevel
	@JsonBackReference(value="card-level")
	@ManyToOne
	@JoinColumn(name="LEVELID")
	public PrefixLevel getPrefixLevel() {
		return this.prefixLevel;
	}

	public void setPrefixLevel(PrefixLevel prefixLevel) {
		this.prefixLevel = prefixLevel;
	}


	//bi-directional many-to-one association to PrefixUserCardStatus
	@JsonManagedReference(value="usercardstatus-card")
	@OneToMany(mappedBy="prefixCard", fetch=FetchType.EAGER)
	public List<PrefixUserCardStatus> getPrefixUserCardStatuses() {
		return this.prefixUserCardStatuses;
	}

	public void setPrefixUserCardStatuses(List<PrefixUserCardStatus> prefixUserCardStatuses) {
		this.prefixUserCardStatuses = prefixUserCardStatuses;
	}

	public PrefixUserCardStatus addPrefixUserCardStatus(PrefixUserCardStatus prefixUserCardStatus) {
		getPrefixUserCardStatuses().add(prefixUserCardStatus);
		prefixUserCardStatus.setPrefixCard(this);

		return prefixUserCardStatus;
	}

	public PrefixUserCardStatus removePrefixUserCardStatus(PrefixUserCardStatus prefixUserCardStatus) {
		getPrefixUserCardStatuses().remove(prefixUserCardStatus);
		prefixUserCardStatus.setPrefixCard(null);

		return prefixUserCardStatus;
	}

}