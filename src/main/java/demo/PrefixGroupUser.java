package demo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;


/**
 * The persistent class for the PREFIX_GROUP_USER database table.
 * 
 */
@Entity
@Table(name="PREFIX_GROUP_USER")
@NamedQuery(name="PrefixGroupUser.findAll", query="SELECT p FROM PrefixGroupUser p")
public class PrefixGroupUser implements Serializable {
	private static final long serialVersionUID = 1L;
	private String groupuserid;
	private String type;
	private PrefixGroup prefixGroup;
	private PrefixStat prefixStat;
	private PrefixUser prefixUser;

	public PrefixGroupUser() {
	}


	@Id
	@Column(name="GROUPUSERID", unique=true, nullable=false, length=20)
	public String getGroupuserid() {
		return this.groupuserid;
	}

	public void setGroupuserid(String groupuserid) {
		this.groupuserid = groupuserid;
	}


	@Column(name="\"TYPE\"", nullable=false, length=200)
	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}


	//bi-directional many-to-one association to PrefixGroup
	@JsonBackReference(value="groupuser-group")
	@ManyToOne
	@JoinColumn(name="GROUPID")
	public PrefixGroup getPrefixGroup() {
		return this.prefixGroup;
	}

	public void setPrefixGroup(PrefixGroup prefixGroup) {
		this.prefixGroup = prefixGroup;
	}


	//bi-directional many-to-one association to PrefixStat
	@JsonBackReference(value="groupuser-stats")
	@ManyToOne
	@JoinColumn(name="STATSID")
	public PrefixStat getPrefixStat() {
		return this.prefixStat;
	}

	public void setPrefixStat(PrefixStat prefixStat) {
		this.prefixStat = prefixStat;
	}


	//bi-directional many-to-one association to PrefixUser
	@JsonBackReference(value="groupuser-user")
	@ManyToOne
	@JoinColumn(name="USERID")
	public PrefixUser getPrefixUser() {
		return this.prefixUser;
	}

	public void setPrefixUser(PrefixUser prefixUser) {
		this.prefixUser = prefixUser;
	}

}