package demo;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;


/**
 * The persistent class for the PREFIX_STATS database table.
 * 
 */
@Entity
@Table(name="PREFIX_STATS")
@NamedQuery(name="PrefixStat.findAll", query="SELECT p FROM PrefixStat p")
public class PrefixStat implements Serializable {
	private static final long serialVersionUID = 1L;
	private long statsid;
	private long courseprogress;
	private long difficultwords;
	private long points;
	private long studytime;
	private long wordshealthyinltm;
	private long wordslearned;
	private List<PrefixGroupUser> prefixGroupUsers;

	public PrefixStat() {
	}


	@Id
	@Column(name="STATSID", unique=true, nullable=false)
	public long getStatsid() {
		return this.statsid;
	}

	public void setStatsid(long statsid) {
		this.statsid = statsid;
	}

//	@Column(name="COURSEPROGRESS")
	public long getCourseprogress() {
		return this.courseprogress;
	}

	public void setCourseprogress(long courseprogress) {
		this.courseprogress = courseprogress;
	}

//	@Column(name="DIFFICULTWORDS")
	public long getDifficultwords() {
		return this.difficultwords;
	}

	public void setDifficultwords(long difficultwords) {
		this.difficultwords = difficultwords;
	}

//	@Column(name="POINTS")
	public long getPoints() {
		return this.points;
	}

	public void setPoints(long points) {
		this.points = points;
	}

//	@Column(name="STUDYTIME")
	public long getStudytime() {
		return this.studytime;
	}

	public void setStudytime(long studytime) {
		this.studytime = studytime;
	}

//	@Column(name="WORDSHEALTHYINLTM")
	public long getWordshealthyinltm() {
		return this.wordshealthyinltm;
	}

	public void setWordshealthyinltm(long wordshealthyinltm) {
		this.wordshealthyinltm = wordshealthyinltm;
	}

//	@Column(name="WORDSLEARNED")
	public long getWordslearned() {
		return this.wordslearned;
	}

	public void setWordslearned(long wordslearned) {
		this.wordslearned = wordslearned;
	}


	//bi-directional many-to-one association to PrefixGroupUser
	@JsonManagedReference(value="groupuser-stats")
	@OneToMany(mappedBy="prefixStat", fetch=FetchType.EAGER)
	public List<PrefixGroupUser> getPrefixGroupUsers() {
		return this.prefixGroupUsers;
	}

	public void setPrefixGroupUsers(List<PrefixGroupUser> prefixGroupUsers) {
		this.prefixGroupUsers = prefixGroupUsers;
	}

	public PrefixGroupUser addPrefixGroupUser(PrefixGroupUser prefixGroupUser) {
		getPrefixGroupUsers().add(prefixGroupUser);
		prefixGroupUser.setPrefixStat(this);

		return prefixGroupUser;
	}

	public PrefixGroupUser removePrefixGroupUser(PrefixGroupUser prefixGroupUser) {
		getPrefixGroupUsers().remove(prefixGroupUser);
		prefixGroupUser.setPrefixStat(null);

		return prefixGroupUser;
	}

}