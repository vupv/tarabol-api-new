package demo;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;


/**
 * The persistent class for the PREFIX_COURSES database table.
 * 
 */
@Entity
@Table(name="PREFIX_COURSES")
@NamedQueries(value = {
@NamedQuery(name="PrefixCours.findAll", query="SELECT p FROM PrefixCours p"),
@NamedQuery(name="PrefixCours.findByID", query="SELECT p FROM PrefixCours p WHERE p.coursesid = :coursesid"),
@NamedQuery(name="PrefixCours.findByMultiID", query="SELECT p FROM PrefixCours p WHERE p.coursesid IN (:coursesid)"),
})

public class PrefixCours implements Serializable {
	private static final long serialVersionUID = 1L;
	private long coursesid;
	private String avatar;
	private String coursesname;
	private String description;
	private String shortdescription;
	private String tags;
	private long teaching;
	private String usedlanguage;
	private List<PrefixCoursesUser> prefixCoursesUsers;
	private List<PrefixLevel> prefixLevels;

	public PrefixCours() {
	}


	@Id
	@Column(name="COURSESID", unique=true, nullable=false)
	public long getCoursesid() {
		return this.coursesid;
	}

	public void setCoursesid(long coursesid) {
		this.coursesid = coursesid;
	}


	@Column(name="AVATAR", length=2000)
	public String getAvatar() {
		return this.avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	@Column(name="COURSESNAME", length=200)
	public String getCoursesname() {
		return this.coursesname;
	}

	public void setCoursesname(String coursesname) {
		this.coursesname = coursesname;
	}


	@Lob @Basic(fetch=FetchType.LAZY)
	@Column(name="DESCRIPTION")
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


	@Lob @Basic(fetch=FetchType.LAZY)
	@Column(name="SHORTDESCRIPTION")
	public String getShortdescription() {
		return this.shortdescription;
	}

	public void setShortdescription(String shortdescription) {
		this.shortdescription = shortdescription;
	}


	@Lob @Basic(fetch=FetchType.LAZY)
	@Column(name="TAGS")
	public String getTags() {
		return this.tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

//	@Column(name="TEACHING")
	public long getTeaching() {
		return this.teaching;
	}

	public void setTeaching(long teaching) {
		this.teaching = teaching;
	}


	@Column(name="USEDLANGUAGE", length=100)
	public String getUsedlanguage() {
		return this.usedlanguage;
	}

	public void setUsedlanguage(String usedlanguage) {
		this.usedlanguage = usedlanguage;
	}


	//bi-directional many-to-one association to PrefixCoursesUser
	@JsonManagedReference(value="coursesuesr-cours")
	@OneToMany(mappedBy="prefixCours", fetch=FetchType.EAGER)
	public List<PrefixCoursesUser> getPrefixCoursesUsers() {
		return this.prefixCoursesUsers;
	}

	public void setPrefixCoursesUsers(List<PrefixCoursesUser> prefixCoursesUsers) {
		this.prefixCoursesUsers = prefixCoursesUsers;
	}

	public PrefixCoursesUser addPrefixCoursesUser(PrefixCoursesUser prefixCoursesUser) {
		getPrefixCoursesUsers().add(prefixCoursesUser);
		prefixCoursesUser.setPrefixCours(this);

		return prefixCoursesUser;
	}

	public PrefixCoursesUser removePrefixCoursesUser(PrefixCoursesUser prefixCoursesUser) {
		getPrefixCoursesUsers().remove(prefixCoursesUser);
		prefixCoursesUser.setPrefixCours(null);

		return prefixCoursesUser;
	}


	//bi-directional many-to-one association to PrefixLevel
	@JsonManagedReference(value="level-cours")
	@OneToMany(mappedBy="prefixCours", fetch=FetchType.EAGER)
	public List<PrefixLevel> getPrefixLevels() {
		return this.prefixLevels;
	}

	public void setPrefixLevels(List<PrefixLevel> prefixLevels) {
		this.prefixLevels = prefixLevels;
	}

	public PrefixLevel addPrefixLevel(PrefixLevel prefixLevel) {
		getPrefixLevels().add(prefixLevel);
		prefixLevel.setPrefixCours(this);

		return prefixLevel;
	}

	public PrefixLevel removePrefixLevel(PrefixLevel prefixLevel) {
		getPrefixLevels().remove(prefixLevel);
		prefixLevel.setPrefixCours(null);

		return prefixLevel;
	}

}