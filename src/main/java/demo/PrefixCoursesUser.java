package demo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;


/**
 * The persistent class for the PREFIX_COURSES_USER database table.
 * 
 */
@Entity
@Table(name="PREFIX_COURSES_USER")
@NamedQuery(name="PrefixCoursesUser.findAll", query="SELECT p FROM PrefixCoursesUser p")
public class PrefixCoursesUser implements Serializable {
	private static final long serialVersionUID = 1L;
	private long coursesuserid;
	private long point;
	private String type;
	private PrefixCours prefixCours;
	private PrefixUser prefixUser;

	public PrefixCoursesUser() {
	}


	@Id
	@Column(name="COURSESUSERID", unique=true, nullable=false)
	public long getCoursesuserid() {
		return this.coursesuserid;
	}

	public void setCoursesuserid(long coursesuserid) {
		this.coursesuserid = coursesuserid;
	}

//	@Column(name="POINT")
	public long getPoint() {
		return this.point;
	}

	public void setPoint(long point) {
		this.point = point;
	}


	@Column(name="\"TYPE\"", length=200)
	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}


	//bi-directional many-to-one association to PrefixCours
	@JsonBackReference(value="coursesuesr-cours")
	@ManyToOne
	@JoinColumn(name="COURSESID")
	public PrefixCours getPrefixCours() {
		return this.prefixCours;
	}

	public void setPrefixCours(PrefixCours prefixCours) {
		this.prefixCours = prefixCours;
	}


	//bi-directional many-to-one association to PrefixUser
	@JsonBackReference(value="coursesuser-user")
	@ManyToOne
	@JoinColumn(name="USERID")
	public PrefixUser getPrefixUser() {
		return this.prefixUser;
	}

	public void setPrefixUser(PrefixUser prefixUser) {
		this.prefixUser = prefixUser;
	}

}