package demo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;


/**
 * The persistent class for the PREFIX_USER_CARD_STATUS database table.
 * 
 */
@Entity
@Table(name="PREFIX_USER_CARD_STATUS")
@NamedQuery(name="PrefixUserCardStatus.findAll", query="SELECT p FROM PrefixUserCardStatus p")
public class PrefixUserCardStatus implements Serializable {
	private static final long serialVersionUID = 1L;
	private long cardstatusid;
	private String status;
	private PrefixCard prefixCard;
	private PrefixUser prefixUser;

	public PrefixUserCardStatus() {
	}


	@Id
	@Column(name="CARDSTATUSID", unique=true, nullable=false)
	public long getCardstatusid() {
		return this.cardstatusid;
	}

	public void setCardstatusid(long cardstatusid) {
		this.cardstatusid = cardstatusid;
	}


	@Column(name="STATUS", length=200)
	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}


	//bi-directional many-to-one association to PrefixCard
	@JsonBackReference(value="usercardstatus-card")
	@ManyToOne
	@JoinColumn(name="CARDID")
	public PrefixCard getPrefixCard() {
		return this.prefixCard;
	}

	public void setPrefixCard(PrefixCard prefixCard) {
		this.prefixCard = prefixCard;
	}


	//bi-directional many-to-one association to PrefixUser
	@JsonBackReference(value="usercardstatus-user")
	@ManyToOne
	@JoinColumn(name="USERID")
	public PrefixUser getPrefixUser() {
		return this.prefixUser;
	}

	public void setPrefixUser(PrefixUser prefixUser) {
		this.prefixUser = prefixUser;
	}

}