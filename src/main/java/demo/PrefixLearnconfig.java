package demo;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;


/**
 * The persistent class for the PREFIX_LEARNCONFIG database table.
 * 
 */
@Entity
@Table(name="PREFIX_LEARNCONFIG")
@NamedQuery(name="PrefixLearnconfig.findAll", query="SELECT p FROM PrefixLearnconfig p")
public class PrefixLearnconfig implements Serializable {
	private static final long serialVersionUID = 1L;
	private long learnconfigid;
	private String audio;
	private long learning;
	private String modes;
	private long reviewing;
	private long tapping;
	private List<PrefixUser> prefixUsers;

	public PrefixLearnconfig() {
	}


	@Id
	@Column(name="LEARNCONFIGID", unique=true, nullable=false)
	public long getLearnconfigid() {
		return this.learnconfigid;
	}

	public void setLearnconfigid(long learnconfigid) {
		this.learnconfigid = learnconfigid;
	}


	@Column(name="AUDIO", length=1000)
	public String getAudio() {
		return this.audio;
	}

	public void setAudio(String audio) {
		this.audio = audio;
	}

	@Column(name="LEARNING")
	public long getLearning() {
		return this.learning;
	}

	public void setLearning(long learning) {
		this.learning = learning;
	}


	@Column(name="MODES", length=100)
	public String getModes() {
		return this.modes;
	}

	public void setModes(String modes) {
		this.modes = modes;
	}

//	@Column(name="REVIEWING")
	public long getReviewing() {
		return this.reviewing;
	}

	public void setReviewing(long reviewing) {
		this.reviewing = reviewing;
	}

//	@Column(name="TAPPING")
	public long getTapping() {
		return this.tapping;
	}

	public void setTapping(long tapping) {
		this.tapping = tapping;
	}


	//bi-directional many-to-one association to PrefixUser
	@JsonManagedReference(value="user-learnconfig")
	@OneToMany(mappedBy="prefixLearnconfig", fetch=FetchType.EAGER)
	public List<PrefixUser> getPrefixUsers() {
		return this.prefixUsers;
	}

	public void setPrefixUsers(List<PrefixUser> prefixUsers) {
		this.prefixUsers = prefixUsers;
	}

	public PrefixUser addPrefixUser(PrefixUser prefixUser) {
		getPrefixUsers().add(prefixUser);
		prefixUser.setPrefixLearnconfig(this);

		return prefixUser;
	}

	public PrefixUser removePrefixUser(PrefixUser prefixUser) {
		getPrefixUsers().remove(prefixUser);
		prefixUser.setPrefixLearnconfig(null);

		return prefixUser;
	}

}