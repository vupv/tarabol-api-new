package com.tarabol.api.el.service;

import java.util.List;

import com.tarabol.dto.DeckUserDto;
import com.tarabol.model.DeckUser;

public interface DeckUserService {
	public List<DeckUser> findDeckUserByUserID(DeckUserDto deckUserDto);
	public List<DeckUser> findDeckByID(DeckUserDto deckUserDto);
	public boolean saveDeckUser(DeckUserDto deckUserDto);
	public boolean deleteDeckUser(DeckUserDto deckUserDto);
}
