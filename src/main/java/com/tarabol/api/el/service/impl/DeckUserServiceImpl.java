package com.tarabol.api.el.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tarabol.api.el.repository.DeckUserRepository;
import com.tarabol.api.el.service.DeckUserService;
import com.tarabol.dto.DeckUserDto;
import com.tarabol.model.DeckUser;

@Service
public class DeckUserServiceImpl implements DeckUserService {

	@Autowired
    private DeckUserRepository deckUserServiceRepository;

	@Override
	public List<DeckUser> findDeckUserByUserID(DeckUserDto deckUserDto) {
		return deckUserServiceRepository.findDeckUserByUserID(deckUserDto.getUserId());
	}

	@Override
	public List<DeckUser> findDeckByID(DeckUserDto deckUserDto) {
		return deckUserServiceRepository.findDeckByID(deckUserDto);
	}

	@Override
	@Transactional
	public boolean saveDeckUser(DeckUserDto deckUserDto) {
		return deckUserServiceRepository.saveDeckUser(deckUserDto);
	}

	@Override
	@Transactional
	public boolean deleteDeckUser(DeckUserDto deckUserDto) {
		return deckUserServiceRepository.deleteDeckUser(deckUserDto);
	}
		

}
