package com.tarabol.api.el.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tarabol.api.el.repository.UserRepository;
import com.tarabol.api.el.service.UserService;
import com.tarabol.dto.UserDto;
import com.tarabol.model.User;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
    private UserRepository userServiceRepository;
	
	@Override
	public List<User> findUser() {
		return userServiceRepository.findUser();
	}

	@Override
	public List<User> findUserByID(long userID) {
		return userServiceRepository.findUserByID(userID);
	}

	@Override
	public boolean checkExistAccount(UserDto userID) {
		return userServiceRepository.checkExistAccount(userID);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public boolean insertUser(UserDto userID) {
		return userServiceRepository.insertUser(userID);
	}

	@Override
	public List<UserDto> login(UserDto userID) {
		return userServiceRepository.login(userID);
	}

	@Override
	public boolean checkAuth(long userId, String userKey) {
		return userServiceRepository.checkAuth(userId, userKey);
	}

	
}
