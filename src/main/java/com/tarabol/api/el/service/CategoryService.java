package com.tarabol.api.el.service;

import java.util.List;

import com.tarabol.dto.CategoryDto;
import com.tarabol.model.Category;

public interface CategoryService {
	
	public List<Category> findCategory();
	public boolean addCategory(CategoryDto categoryDto);
	public boolean editCategory(CategoryDto categoryDto);
	public boolean deleteCategory(CategoryDto categoryDto);
}
