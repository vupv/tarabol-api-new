package com.tarabol.api.el.service;

import java.util.List;

import com.tarabol.dto.UserDto;
import com.tarabol.model.User;

public interface UserService {
	
	public List<User> findUser();
	public List<User> findUserByID(long userID);
	public boolean checkExistAccount(UserDto userID);
	public boolean insertUser(UserDto userID);
	public List<UserDto> login(UserDto userID);
	public boolean checkAuth(long userId, String userKey);
}
