package com.tarabol.api.el.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tarabol.api.el.repository.FileRepository;
import com.tarabol.api.el.service.FileService;

@Service
public class FileServiceImpl implements FileService {

	@Autowired
	private FileRepository fileRepository;

	@Override
	public String saveFileToStore(byte[] datas, String fileName) {
		return fileRepository.saveFileToStore(datas, fileName);
	}

}
