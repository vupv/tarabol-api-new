package com.tarabol.api.el.service;

import java.util.List;

import com.tarabol.dto.LessonDto;
import com.tarabol.model.Lesson;

public interface LessonService {
	public List<Lesson> findLessonByID(long l);
	public List<Lesson> findLessonByDeckID(long deckID);
	public boolean saveLesson(LessonDto lessonDto);
	public boolean deleteLesson(LessonDto lessonDto);
}
