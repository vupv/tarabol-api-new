package com.tarabol.api.el.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tarabol.api.el.repository.DeckRepository;
import com.tarabol.api.el.service.DeckService;
import com.tarabol.dto.DeckDto;
import com.tarabol.dto.UserDto;
import com.tarabol.model.Deck;

@Service
public class DeckServiceImpl implements DeckService {

	@Autowired
	private DeckRepository deckServiceRepository;

	@Override
	public List<Deck> findDeck() {
		return deckServiceRepository.findDeck();
	}

	@Override
	public List<Deck> findDeckByUser(UserDto userDto) {
		return deckServiceRepository.findDeckByUser(userDto.getUserId(), userDto.getFirst(), userDto.getMax());
	}

	@Override
	public List<Deck> findDeckTop(int top) {
		return deckServiceRepository.findDeckTop(top);
	}

	@Override
	public List<Deck> findDeckByID(long deckID) {
		return deckServiceRepository.findDeckByID(deckID);
	}

	@Override
	@Transactional
	public boolean saveDeck(DeckDto deckDto) {
		return deckServiceRepository.saveDeck(deckDto);
	}

	@Override
	public boolean deleteDeck(DeckDto deckDto) {
		return deckServiceRepository.deleteDeck(deckDto);
	}

	@Override
	public long countFindAll() {
		// TODO Auto-generated method stub
		return deckServiceRepository.countFindAll();
	}

	@Override
	public List<DeckDto> findBestNew(int page, int quantity) {
		return deckServiceRepository.findBestNew(page, quantity);
	}

	@Override
	public List<DeckDto> findPopular(int page, int quantity) {
		
		return deckServiceRepository.findPopular(page, quantity);
	}

	@Override
	public List<DeckDto> find(String freeword, long categoryId, int page, int quantity) {
		
		return deckServiceRepository.find(freeword, categoryId, page, quantity);
	}

	@Override
	public long countFind(String freeword, long categoryId) {
		
		return deckServiceRepository.countFind(freeword, categoryId);
	}
	

}
