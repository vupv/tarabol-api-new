package com.tarabol.api.el.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tarabol.api.el.repository.CardRepository;
import com.tarabol.api.el.service.CardService;
import com.tarabol.dto.CardDto;
import com.tarabol.model.Card;

@Service
public class CardServiceImpl implements CardService {

	@Autowired
    private CardRepository cardServiceRepository;

	@Override
	public List<Card> findCardByID(CardDto cardDto) {
		return cardServiceRepository.findCardByID(cardDto);
	}

	@Override
	@Transactional
	public boolean saveCard(CardDto cardDto) {
		return cardServiceRepository.saveCard(cardDto);
	}

	@Override
	@Transactional
	public boolean deleteCard(CardDto cardDto) {
		return cardServiceRepository.deleteCard(cardDto);
	}

	@Override
	public List<Card> findCardByLessonID(long lessonID) {
		return cardServiceRepository.findCardByLessonID(lessonID);
	}

}
