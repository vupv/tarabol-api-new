package com.tarabol.api.el.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tarabol.api.el.repository.LessonRepository;
import com.tarabol.api.el.service.LessonService;
import com.tarabol.dto.LessonDto;
import com.tarabol.model.Lesson;

@Service
public class LessonServiceImpl implements LessonService {

	@Autowired
    private LessonRepository lessonServiceRepository;

	@Override
	public List<Lesson> findLessonByID(long lessonID) {
		return lessonServiceRepository.findLessonByID(lessonID);
	}

	@Override
	@Transactional
	public boolean saveLesson(LessonDto lessonDto) {
		return lessonServiceRepository.saveLesson(lessonDto);
	}

	@Override
	@Transactional
	public boolean deleteLesson(LessonDto lessonDto) {
		return lessonServiceRepository.deleteLesson(lessonDto);
	}

	@Override
	public List<Lesson> findLessonByDeckID(long deckID) {
		return lessonServiceRepository.findLessonByDeckID(deckID);
	}
	
	
}
