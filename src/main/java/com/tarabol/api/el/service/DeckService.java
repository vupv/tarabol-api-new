package com.tarabol.api.el.service;

import java.util.List;

import com.tarabol.dto.DeckDto;
import com.tarabol.dto.UserDto;
import com.tarabol.model.Deck;

public interface DeckService {
	public List<Deck> findDeck();

	public List<Deck> findDeckByUser(UserDto userDto);

	public List<Deck> findDeckTop(int top);

	public List<Deck> findDeckByID(long deckId);

	public boolean saveDeck(DeckDto deckDto);

	public boolean deleteDeck(DeckDto deckDto);

	public long countFindAll();
	
	public List<DeckDto> findBestNew(int page, int quantity);
	
	public List<DeckDto> findPopular(int page, int quantity);
	
	public List<DeckDto> find(String freeword, long categoryId, int page, int quantity);
	public long countFind(String freeword, long categoryId);
	

}
