package com.tarabol.api.el.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tarabol.api.el.repository.CategoryRepository;
import com.tarabol.api.el.service.CategoryService;
import com.tarabol.dto.CategoryDto;
import com.tarabol.model.Category;

@Service
public class CategoryServiceImpl implements CategoryService {

	@Autowired
    private CategoryRepository categoryServiceRepository;
	
	@Override
	public List<Category> findCategory() {
		try {
			return categoryServiceRepository.findCategory();	
		} catch (Exception e) {
			return null; 
		}
		
	}

	@Override
	@Transactional
	public boolean addCategory(CategoryDto categoryDto) {
		return categoryServiceRepository.addCategory(categoryDto);
	}

	@Override
	@Transactional
	public boolean deleteCategory(CategoryDto categoryDto) {
		return categoryServiceRepository.deleteCategory(categoryDto);
	}

	@Override
	@Transactional
	public boolean editCategory(CategoryDto categoryDto) {
		return categoryServiceRepository.editCategory(categoryDto);
	}
}
