package com.tarabol.api.el.service;

import java.util.List;

import com.tarabol.dto.CardDto;
import com.tarabol.model.Card;

public interface CardService {
	public List<Card> findCardByID(CardDto cardDto);
	public List<Card> findCardByLessonID(long lessonID);
	public boolean saveCard(CardDto cardDto);
	public boolean deleteCard(CardDto cardDto);
}
