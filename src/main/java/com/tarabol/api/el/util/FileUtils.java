package com.tarabol.api.el.util;

import java.io.IOException;

import org.springframework.web.multipart.MultipartFile;

public class FileUtils {
	public static byte[] getBytesFromMultiPartFile(MultipartFile mFile) {
		byte[] bytes;
		try {
			bytes = mFile.getBytes();
		} catch (IOException e) {
			e.printStackTrace();
			bytes = new byte[0];
		}
		return bytes;
	}
}
