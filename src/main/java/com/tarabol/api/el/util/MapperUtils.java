package com.tarabol.api.el.util;

import com.fasterxml.jackson.databind.ObjectMapper;

public class MapperUtils {
	public static <T> T convert(Object obj, Class<T> type) {
		ObjectMapper mapper = new ObjectMapper();
		return mapper.convertValue(obj, type);
	}
}
