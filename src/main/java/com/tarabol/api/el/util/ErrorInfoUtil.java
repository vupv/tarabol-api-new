package com.tarabol.api.el.util;

import java.util.Map;

public class ErrorInfoUtil {

	public final static int CODE_SUCCESS = 0;
	public final static int CODE_CAN_NOT_VALIDATE_INPUT_DATA = 101;
	public final static int CODE_USER_ID_KEY_NOT_MATCH = 102;
	public final static int CODE_APPID_NOT_EXIST = 103;
	public final static int CODE_USERNAME_EMAIL_ALREADY_EXIST = 104;
	public final static int CODE_USERNAME_PASSWORD_WRONG = 105;
	public final static int CODE_DATA_OBJECT_EXIST = 106;
	public final static int CODE_DATA_INPUT_IS_DUPLICATE = 107;
	public final static int CODE_SYSTEM_ERROR = 201;
	public final static int CODE_NOT_CONNECT_TO_DATA = 301;
	public final static int CODE_OBJECT_NOT_FOUND = 302;
	

	public static Map<Integer, String> errorMap;

	public static String getErrorDesc(int code) {
		
		return errorMap.get(code);
	}
}

