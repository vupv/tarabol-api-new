package com.tarabol.api.el.util;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.KeySpec;
import java.util.UUID;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;

public class MyNumberUtil {
	
	private static String secretKey = "Bar12345Bar12345";// 128 bit key
	
	public static String getRandomKey() {
		return UUID.randomUUID().toString();
	}
	
	public static String encryptMD5(String input) {
		//Create MessageDigest object for MD5
		MessageDigest digest = null;
		try {
			digest = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		
		//Update input string in message digest
		input = input + "fsdfhsflsdfksdhhskfhsdkfshks";
		digest.update(input.getBytes(), 0, input.length());
	
		//Converts message digest value in base 16 (hex) 
		String md5 = new BigInteger(1, digest.digest()).toString(16);
		return md5;
	}
	
	private static Cipher ecipher;
	private static Cipher dcipher;
    // 8-byte Salt
	private static byte[] salt = {
        (byte) 0xA9, (byte) 0x9B, (byte) 0xC8, (byte) 0x32,
        (byte) 0x56, (byte) 0x35, (byte) 0xE3, (byte) 0x03
    };
    // Iteration count
	private static int iterationCount = 19;

    /**
     * 
     * @param secretKey Key used to encrypt data
     * @param plainText Text input to be encrypted
     * @return Returns encrypted text
     * 
     */
    @SuppressWarnings("restriction")
	public static String encrypt(String plainText) {
    	try {
	        //Key generation for enc and desc
	        KeySpec keySpec = new PBEKeySpec(secretKey.toCharArray(), salt, iterationCount);
	        SecretKey key = SecretKeyFactory.getInstance("PBEWithMD5AndDES").generateSecret(keySpec);        
	         // Prepare the parameter to the ciphers
	        AlgorithmParameterSpec paramSpec = new PBEParameterSpec(salt, iterationCount);
	
	        //Enc process
	        ecipher = Cipher.getInstance(key.getAlgorithm());
	        ecipher.init(Cipher.ENCRYPT_MODE, key, paramSpec);      
	        String charSet="UTF-8";       
	        byte[] in = plainText.getBytes(charSet);
	        byte[] out = ecipher.doFinal(in);
			String encStr=new sun.misc.BASE64Encoder().encode(out);
	        return encStr;
    	} catch (Exception e) {
    		
    	}
    	return "";
    }
     /**     
     * @param secretKey Key used to decrypt data
     * @param encryptedText encrypted text input to decrypt
     * @return Returns plain text after decryption
     */
    @SuppressWarnings("restriction")
	public static String decrypt(String encryptedText) {
    	try {
	         //Key generation for enc and desc
	        KeySpec keySpec = new PBEKeySpec(secretKey.toCharArray(), salt, iterationCount);
	        SecretKey key = SecretKeyFactory.getInstance("PBEWithMD5AndDES").generateSecret(keySpec);        
	         // Prepare the parameter to the ciphers
	        AlgorithmParameterSpec paramSpec = new PBEParameterSpec(salt, iterationCount);
	        //Decryption process; same key will be used for decr
	        dcipher=Cipher.getInstance(key.getAlgorithm());
	        dcipher.init(Cipher.DECRYPT_MODE, key,paramSpec);
	        byte[] enc = new sun.misc.BASE64Decoder().decodeBuffer(encryptedText);
	        byte[] utf8 = dcipher.doFinal(enc);
	        String charSet="UTF-8";     
	        String plainStr = new String(utf8, charSet);
	        return plainStr;
    	} catch(Exception e) {
    		
    	}
    	return "";
    }   
    
    public static void main(String args[]) {
    	System.out.println(encryptMD5("123456"));
    }
}
