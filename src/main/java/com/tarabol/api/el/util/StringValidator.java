package com.tarabol.api.el.util;

import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.validator.routines.DateValidator;
import org.apache.commons.validator.routines.EmailValidator;

public class StringValidator {

	public static boolean eMailValidator(String email) {
		if(email == null){
			return false;
		}
		return EmailValidator.getInstance().isValid(email);
	}

	public static boolean dateValidator(String date, String pattern) {
		if(date == null){
			return false;
		}
		return DateValidator.getInstance().isValid(date, pattern);
	}

	public static boolean numberValidate(String number) {
		if(number == null){
			return false;
		}
		return NumberUtils.isNumber(number);
	}

	public static boolean idValidator(long id) {
		return id > 0;
	}
	
	public static boolean fieldNotNull(String value){
		if(value == null){
			return false;
		}
		return !value.isEmpty();
	}
//	
//	public static List<String> getDuplicateObj(List<SubsDto> subsDtoList) {
//		
//		// Set of username duplicate
//		Set<String> existSet = new TreeSet<>();
//
//		// Set of visit username
//		Set<String> visitUsernameSet = new TreeSet<>();
//		// Set of visit id 
//		Set<Long> visitIdSet = new TreeSet<>();
//
//		for (SubsDto subsDto : subsDtoList) {
//			// Check duplicate username
//			if (visitUsernameSet.contains(subsDto.getUsername())) {
//				existSet.add(subsDto.getUsername());
//			} else {
//				visitUsernameSet.add(subsDto.getUsername());
//			}
//				
//			// Check duplicate id
//			if (subsDto.getSubId() != 0 && visitIdSet.contains(subsDto.getSubId())) {
//				existSet.add(subsDto.getUsername());
//			} else {
//				visitIdSet.add(subsDto.getSubId());
//			}
//		}
//		
//		if (CollectionUtils.isNotEmpty(existSet)) {
//			return new ArrayList<String>(existSet);
//		}
//		
//		return null;
//		
//	}
	
}
