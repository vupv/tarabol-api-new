package com.tarabol.api.el.response;

import java.util.List;

public class ResponseCommon {
	@SuppressWarnings("rawtypes")
	private List bodyResponse;
	private int errorCode;
	private String erorrDecreption;
	private boolean result;
	
	@SuppressWarnings("rawtypes")
	public ResponseCommon(List bodyResponse, int errorCode,
			String erorrDecreption, boolean result) {
		super();
		this.bodyResponse = bodyResponse;
		this.errorCode = errorCode;
		this.erorrDecreption = erorrDecreption;
		this.result = result;
	}

	public ResponseCommon() {
		super();
	}

	/**
	 * @return the obj
	 */
	@SuppressWarnings("rawtypes")
	public List getBodyResponse() {
		return bodyResponse;
	}

	/**
	 * @param obj the obj to set
	 */
	@SuppressWarnings("rawtypes")
	public void setBodyResponse(List bodyResponse) {
		this.bodyResponse = bodyResponse;
	}

	/**
	 * @return the errorCode
	 */
	public int getErrorCode() {
		return errorCode;
	}

	/**
	 * @param errorCode the errorCode to set
	 */
	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}

	/**
	 * @return the erorrDecreption
	 */
	public String getErorrDecreption() {
		return erorrDecreption;
	}

	/**
	 * @param erorrDecreption the erorrDecreption to set
	 */
	public void setErorrDecreption(String erorrDecreption) {
		this.erorrDecreption = erorrDecreption;
	}

	/**
	 * @return the result
	 */
	public boolean isResult() {
		return result;
	}

	/**
	 * @param result the result to set
	 */
	public void setResult(boolean result) {
		this.result = result;
	}
	
}
