package com.tarabol.api.el.shiro;

import org.apache.log4j.Logger;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SaltedAuthenticationInfo;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.realm.jdbc.JdbcRealm;

import com.tarabol.dto.SaltedAuthenticationInfoDto;



/**
 * This class instead of default Shiro's Realm. Get SaltedAuthenticationInfo
 * using for authentication account
 * 
 * @author vupv4
 */
public class CustomShiroRealm extends JdbcRealm {

	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {

		/* Identify account to log to */
		UsernamePasswordToken userPassToken = (UsernamePasswordToken) token;
		final String userName = userPassToken.getUsername();

		if (userName == null) {
			Logger logger = Logger.getLogger(this.getClass());
			logger.error("Error on getting User name!");
			return null;
		}

		/* Read password hash and salt from database */
		final SaltedAuthenticationInfoDto userSalted = new SaltedAuthenticationInfoDto();
		// VUPV hardcode test login
		userSalted.setPassword("123456");
		userSalted.setSalt("xdsEWWSdsdsdwredaaa");
		
		/* Check user authentication information is correct? If not throw exception */
		SaltedAuthenticationInfo info = new MySaltedAuthentificationInfo(userName, userSalted.getPassword(), userSalted.getSalt());

		return info;
	}
}
