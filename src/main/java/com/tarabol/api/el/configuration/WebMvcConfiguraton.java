/**
 * 
 */
package com.tarabol.api.el.configuration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.WebSecurityManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.hibernate4.Hibernate4Module;

/**
 * @author TuanAnhIT
 *
 */
public class WebMvcConfiguraton extends WebMvcConfigurerAdapter {

	@Override
	public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
		converters.add(jacksonMessageConverter());
		super.configureMessageConverters(converters);
	}

	// get a configured Hibernate4Module
	// here as an example with a disabled USE_TRANSIENT_ANNOTATION feature
	private Hibernate4Module hibernate4Module() {
		return new Hibernate4Module().disable(Hibernate4Module.Feature.USE_TRANSIENT_ANNOTATION);
	}

	// create the ObjectMapper with Spring's Jackson2ObjectMapperBuilder
	// and passing the hibernate4Module to modulesToInstall()
	private MappingJackson2HttpMessageConverter jacksonMessageConverter() {
		Jackson2ObjectMapperBuilder builder = new Jackson2ObjectMapperBuilder()
				.featuresToDisable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS).modulesToInstall(hibernate4Module());
		return new MappingJackson2HttpMessageConverter(builder.build());
	}
}
