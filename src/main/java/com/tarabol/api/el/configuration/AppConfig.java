package com.tarabol.api.el.configuration;

import java.io.IOException;
import java.nio.file.Files;
import java.util.Properties;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.spi.PersistenceProvider;
import javax.sql.DataSource;

import org.hibernate.jpa.HibernatePersistenceProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate4.HibernateExceptionTranslator;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories("com.tarabol.api.el.repository")
@PropertySource("classpath:application.properties")
@EnableWebMvc
@ComponentScan(basePackages = "com.tarabol.api.el")
public class AppConfig {

	private static final String PROPERTY_NAME_DATABASE_DRIVER = "db.driver";
	private static final String PROPERTY_NAME_DATABASE_PASSWORD = "db.password";
	private static final String PROPERTY_NAME_DATABASE_URL = "db.url";
	private static final String PROPERTY_NAME_DATABASE_USERNAME = "db.username";
	private static final String PROPERTY_NAME_HIBERNATE_DIALECT = "hibernate.dialect";
	private static final String PROPERTY_NAME_HIBERNATE_SHOW_SQL = "hibernate.show_sql";
	private static final String PROPERTY_NAME_HIBERNATE_CHARSET = "hibernate.connection.charSet";
	private static final String PROPERTY_NAME_JBOSS_MANAGED = "jboss.as.jpa.managed";
	private static final String PROPERTY_NAME_PERSISTENCEUNITNAME = "persistence.unitname";
	private static final String PROPERTY_NAME_ENTITYMANAGER_PACKAGES_TO_SCAN = "entitymanager.packages.to.scan";
	public static final String PROPERTY_NAME_MAX_RECORD = "app.max_record";

	@Autowired
	private Environment env;

	@Bean
	public CommonsMultipartResolver multipartResolver() {
		CommonsMultipartResolver commonsMultipartResolver = new CommonsMultipartResolver();
		commonsMultipartResolver.setDefaultEncoding("utf-8");
		commonsMultipartResolver.setMaxUploadSize(50000000);
		return commonsMultipartResolver;
	}

	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactory() {

		HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		vendorAdapter.setShowSql(true);
		vendorAdapter.setGenerateDdl(true);

		LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();

		entityManagerFactoryBean.setJpaVendorAdapter(vendorAdapter);
		entityManagerFactoryBean.setDataSource(dataSource());
		entityManagerFactoryBean.setPackagesToScan(env.getRequiredProperty(PROPERTY_NAME_ENTITYMANAGER_PACKAGES_TO_SCAN));
		entityManagerFactoryBean.setPersistenceProvider(persistenceProvider());
		entityManagerFactoryBean.setPersistenceUnitName(env.getRequiredProperty(PROPERTY_NAME_PERSISTENCEUNITNAME));

		Properties jpaProperties = buildProperties();
		entityManagerFactoryBean.setJpaProperties(jpaProperties);
		entityManagerFactoryBean.afterPropertiesSet();
		return entityManagerFactoryBean;
	}

	@Bean
	public EntityManager entityManager(EntityManagerFactory entityManagerFactory) {
		return entityManagerFactory.createEntityManager();
	}

	@Bean
	public PersistenceProvider persistenceProvider() {
		return new HibernatePersistenceProvider();
	}

	@Bean
	public DataSource dataSource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName(env.getRequiredProperty(PROPERTY_NAME_DATABASE_DRIVER));
		dataSource.setUrl(env.getRequiredProperty(PROPERTY_NAME_DATABASE_URL));
		dataSource.setUsername(env.getRequiredProperty(PROPERTY_NAME_DATABASE_USERNAME));
		dataSource.setPassword(env.getRequiredProperty(PROPERTY_NAME_DATABASE_PASSWORD));

		return dataSource;
	}

	@Bean
	public LocalSessionFactoryBean sessionFactory() {
		LocalSessionFactoryBean sessionFactoryBean = new LocalSessionFactoryBean();
		sessionFactoryBean.setDataSource(dataSource());
		sessionFactoryBean.setPackagesToScan(env.getRequiredProperty(PROPERTY_NAME_ENTITYMANAGER_PACKAGES_TO_SCAN));
		sessionFactoryBean.setHibernateProperties(buildProperties());

		return sessionFactoryBean;
	}

	private Properties buildProperties() {
		Properties properties = new Properties();
		properties.put(PROPERTY_NAME_HIBERNATE_DIALECT, env.getRequiredProperty(PROPERTY_NAME_HIBERNATE_DIALECT));
		properties.put(PROPERTY_NAME_HIBERNATE_SHOW_SQL, env.getRequiredProperty(PROPERTY_NAME_HIBERNATE_SHOW_SQL));
		properties.put(PROPERTY_NAME_HIBERNATE_CHARSET, env.getRequiredProperty(PROPERTY_NAME_HIBERNATE_CHARSET));
		properties.put(PROPERTY_NAME_JBOSS_MANAGED, env.getRequiredProperty(PROPERTY_NAME_JBOSS_MANAGED));

		return properties;
	}

	@Bean
	public JpaTransactionManager transactionManager() {
		JpaTransactionManager jpaTransactionManager = new JpaTransactionManager();
		jpaTransactionManager.setEntityManagerFactory(entityManagerFactory().getObject());
		return jpaTransactionManager;
	}

	@Bean
	public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
		return new PersistenceExceptionTranslationPostProcessor();
	}

	@Bean
	public HibernateExceptionTranslator hibernateExceptionTranslator() {
		return new HibernateExceptionTranslator();
	}

	public static String SQL_LOGIN = "";
	public static String SQL_GET_USER = "";
	public static String SQL_GET_USER_BY_ID = "";
	public static String SQL_GET_DECK = "";
	public static String SQL_GET_DECK_BY_ID = "";
	public static String SQL_GET_DECK_BY_MULTI_ID = "";
	public static String SQL_GET_CATEGORY = "";
	public static String SQL_ADD_CATEGORY = "";
	public static String SQL_UPDATE_CATEGORY = "";
	public static String SQL_DELETE_CATEGORY = "";
	public static String SQL_UPDATE_USER = "";
	public static String SQL_DELETE_USER = "";
	public static String SQL_GET_DECK_BY_USER = "";
	public static String SQL_GET_CARD_BY_ID = "";
	public static String SQL_GET_CARD_BY_LESSON_ID = "";
	public static String SQL_ADD_CARD = "";
	public static String SQL_UPDATE_CARD = "";
	public static String SQL_DELETE_CARD = "";
	public static String SQL_ADD_DECK = "";
	public static String SQL_UPDATE_DECK = "";
	public static String SQL_DELETE_DECK = "";
	public static String SQL_GET_DECKUSER_BY_ID = "";
	public static String SQL_GET_DECKUSER_BY_USER_ID = "";
	public static String SQL_ADD_DECKUSER = "";
	public static String SQL_UPDATE_DECKUSER = "";
	public static String SQL_DELETE_DECKUSER = "";
	public static String SQL_GET_LESSON_BY_ID = "";
	public static String SQL_GET_LESSON_BY_DECK_ID = "";
	public static String SQL_ADD_LESSON = "";
	public static String SQL_UPDATE_LESSON = "";
	public static String SQL_DELETE_LESSON = "";
	public static String SQL_COUNT_FIND_ALL_DECK="";

	@Value("classpath:sql/APP_GET_USER.sql")
	private Resource appGetUserResource;

	@Value("classpath:sql/APP_GET_USER_BY_ID.sql")
	private Resource appGetUserByIDResource;

	@Value("classpath:sql/APP_GET_DECK.sql")
	private Resource appGetDeckResource;

	@Value("classpath:sql/APP_GET_DECK_BY_ID.sql")
	private Resource appGetDeckByIDResource;

	@Value("classpath:sql/APP_GET_DECK_BY_MULTI_ID.sql")
	private Resource appGetDeckByMultiIDResource;

	@Value("classpath:sql/APP_GET_CATEGORY.sql")
	private Resource appGetCategoryResource;

	@Value("classpath:sql/APP_ADD_CATEGORY.sql")
	private Resource appAddCategoryResource;

	@Value("classpath:sql/APP_UPDATE_CATEGORY.sql")
	private Resource appEditCategoryResource;

	@Value("classpath:sql/APP_DELETE_CATEGORY.sql")
	private Resource appDelCategoryResource;

	@Value("classpath:sql/APP_LOGIN.sql")
	private Resource appLoginResource;

	@Value("classpath:sql/APP_UPDATE_USER.sql")
	private Resource appUpdateUserResource;

	@Value("classpath:sql/APP_DELETE_USER.sql")
	private Resource appDeleteUserResource;

	@Value("classpath:sql/APP_GET_DECK_BY_USER.sql")
	private Resource appGetDeckByUserResource;

	@Value("classpath:sql/APP_GET_CARD_BY_ID.sql")
	private Resource appGetCardByIDResource;

	@Value("classpath:sql/APP_GET_CARD_BY_LESSON_ID.sql")
	private Resource appGetCardByLessonIDResource;

	@Value("classpath:sql/APP_ADD_CARD.sql")
	private Resource appAddCardResource;

	@Value("classpath:sql/APP_UPDATE_CARD.sql")
	private Resource appUpdateCardResource;

	@Value("classpath:sql/APP_DELETE_CARD.sql")
	private Resource appDeleteCardResource;

	@Value("classpath:sql/APP_ADD_DECK.sql")
	private Resource appAddDeckResource;

	@Value("classpath:sql/APP_UPDATE_DECK.sql")
	private Resource appUpdateDeckResource;

	@Value("classpath:sql/APP_DELETE_DECK.sql")
	private Resource appDeleteDeckResource;

	@Value("classpath:sql/APP_GET_DECKUSER_BY_ID.sql")
	private Resource appGetDeckUserByIDResource;

	@Value("classpath:sql/APP_GET_DECKUSER_BY_USER_ID.sql")
	private Resource appGetDeckUserByUserIDResource;

	@Value("classpath:sql/APP_ADD_DECKUSER.sql")
	private Resource appAddDeckUserResource;

	@Value("classpath:sql/APP_UPDATE_DECKUSER.sql")
	private Resource appUpdateDeckUserResource;

	@Value("classpath:sql/APP_DELETE_DECKUSER.sql")
	private Resource appDeleteDeckUserResource;

	@Value("classpath:sql/APP_ADD_LESSON.sql")
	private Resource appAddLessonResource;

	@Value("classpath:sql/APP_UPDATE_LESSON.sql")
	private Resource appUpdateLessonResource;

	@Value("classpath:sql/APP_DELETE_LESSON.sql")
	private Resource appDeleteLessonResource;

	@Value("classpath:sql/APP_GET_LESSON_BY_ID.sql")
	private Resource appGetLessonByIDResource;

	@Value("classpath:sql/APP_GET_LESSON_BY_DECK_ID.sql")
	private Resource appGetLessonByDeckIDResource;

	@Value("classpath:sql/APP_COUNT_FIND_ALL_DECK.sql")
	private Resource appCountFindAllDeckIDResource;
	
	@Bean
	public String loadSql() throws IOException {
		SQL_GET_USER = new String(Files.readAllBytes(appGetUserResource.getFile().toPath()));
		SQL_GET_USER_BY_ID = new String(Files.readAllBytes(appGetUserByIDResource.getFile().toPath()));
		SQL_GET_DECK = new String(Files.readAllBytes(appGetDeckResource.getFile().toPath()));
		SQL_GET_DECK_BY_ID = new String(Files.readAllBytes(appGetDeckByIDResource.getFile().toPath()));
		SQL_GET_DECK_BY_MULTI_ID = new String(Files.readAllBytes(appGetDeckByMultiIDResource.getFile().toPath()));
		SQL_GET_CATEGORY = new String(Files.readAllBytes(appGetCategoryResource.getFile().toPath()));
		SQL_ADD_CATEGORY = new String(Files.readAllBytes(appAddCategoryResource.getFile().toPath()));
		SQL_UPDATE_CATEGORY = new String(Files.readAllBytes(appEditCategoryResource.getFile().toPath()));
		SQL_DELETE_CATEGORY = new String(Files.readAllBytes(appDelCategoryResource.getFile().toPath()));
		SQL_LOGIN = new String(Files.readAllBytes(appLoginResource.getFile().toPath()));
		SQL_UPDATE_USER = new String(Files.readAllBytes(appUpdateUserResource.getFile().toPath()));
		SQL_DELETE_USER = new String(Files.readAllBytes(appDeleteUserResource.getFile().toPath()));
		SQL_GET_DECK_BY_USER = new String(Files.readAllBytes(appGetDeckByUserResource.getFile().toPath()));
		SQL_GET_CARD_BY_ID = new String(Files.readAllBytes(appGetCardByIDResource.getFile().toPath()));
		SQL_GET_CARD_BY_LESSON_ID = new String(Files.readAllBytes(appGetCardByLessonIDResource.getFile().toPath()));
		SQL_ADD_CARD = new String(Files.readAllBytes(appAddCardResource.getFile().toPath()));
		SQL_UPDATE_CARD = new String(Files.readAllBytes(appUpdateCardResource.getFile().toPath()));
		SQL_DELETE_CARD = new String(Files.readAllBytes(appDeleteCardResource.getFile().toPath()));
		SQL_ADD_DECK = new String(Files.readAllBytes(appAddDeckResource.getFile().toPath()));
		SQL_UPDATE_DECK = new String(Files.readAllBytes(appUpdateDeckResource.getFile().toPath()));
		SQL_DELETE_DECK = new String(Files.readAllBytes(appDeleteDeckResource.getFile().toPath()));
		SQL_GET_DECKUSER_BY_ID = new String(Files.readAllBytes(appGetDeckUserByIDResource.getFile().toPath()));
		SQL_GET_DECKUSER_BY_USER_ID = new String(Files.readAllBytes(appGetDeckUserByUserIDResource.getFile().toPath()));
		SQL_ADD_DECKUSER = new String(Files.readAllBytes(appAddDeckUserResource.getFile().toPath()));
		SQL_UPDATE_DECKUSER = new String(Files.readAllBytes(appUpdateDeckUserResource.getFile().toPath()));
		SQL_DELETE_DECKUSER = new String(Files.readAllBytes(appDeleteDeckUserResource.getFile().toPath()));
		SQL_ADD_LESSON = new String(Files.readAllBytes(appAddLessonResource.getFile().toPath()));
		SQL_UPDATE_LESSON = new String(Files.readAllBytes(appUpdateLessonResource.getFile().toPath()));
		SQL_DELETE_LESSON = new String(Files.readAllBytes(appDeleteLessonResource.getFile().toPath()));
		SQL_GET_LESSON_BY_ID = new String(Files.readAllBytes(appGetLessonByIDResource.getFile().toPath()));
		SQL_GET_LESSON_BY_DECK_ID = new String(Files.readAllBytes(appGetLessonByDeckIDResource.getFile().toPath()));
		SQL_COUNT_FIND_ALL_DECK = new String(Files.readAllBytes(appCountFindAllDeckIDResource.getFile().toPath()));

		return null;
	}
}