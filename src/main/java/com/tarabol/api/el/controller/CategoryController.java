package com.tarabol.api.el.controller;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tarabol.api.el.service.CategoryService;
import com.tarabol.dto.CategoryDto;
import com.tarabol.model.Category;
import com.tarabol.response.ResponseCommon;

@Controller
@RequestMapping(value = "/category", produces = "application/json;charset=UTF-8")
public class CategoryController {

	@Autowired
	private CategoryService categoryService;
	
	private ResponseCommon responseCommon;
	
	@RequestMapping(value = "/findAll", method = RequestMethod.GET)
	@ResponseBody
	public ResponseCommon findAll() {
		responseCommon = new ResponseCommon();
		List<Category> result = categoryService.findCategory();
		if (!CollectionUtils.isEmpty(result)) {
			responseCommon.setBodyResponse(result);
			responseCommon.setResult(true);
		} else{
			responseCommon.setErrorCode(101);
			responseCommon.setErorrDecreption("OBJECT NOT FOUND");
			responseCommon.setResult(false);
		}
		return responseCommon;
	}
	
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	@ResponseBody
	public ResponseCommon addCategory(@RequestBody CategoryDto categoryDto) {
		responseCommon = new ResponseCommon();
		try {
			if(categoryService.addCategory(categoryDto)){
				responseCommon.setResult(true);
			} else{
				responseCommon.setErrorCode(201);
				responseCommon.setErorrDecreption("SERVICE ERROR");
				responseCommon.setResult(false);
			}
		} catch (Exception e) {
			responseCommon.setErrorCode(201);
			responseCommon.setErorrDecreption("SERVICE ERROR");
			responseCommon.setResult(false);
		}
		
		return responseCommon;
	}
	
	@RequestMapping(value = "/edit", method = RequestMethod.POST)
	@ResponseBody
	public ResponseCommon editCategory(@RequestBody CategoryDto categoryDto) {
		responseCommon = new ResponseCommon();
		try {
			if(categoryService.editCategory(categoryDto)){
				responseCommon.setResult(true);
			} else{
				responseCommon.setErrorCode(201);
				responseCommon.setErorrDecreption("SERVICE ERROR");
				responseCommon.setResult(false);
			}
		} catch (Exception e) {
			responseCommon.setErrorCode(201);
			responseCommon.setErorrDecreption("SERVICE ERROR");
			responseCommon.setResult(false);
		}
	
		return responseCommon;
	}
	
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@ResponseBody
	public ResponseCommon delCategory(@RequestBody CategoryDto categoryDto) {
		responseCommon = new ResponseCommon();
		try {
			if(categoryService.deleteCategory(categoryDto)){
				responseCommon.setResult(true);
			}else{
				responseCommon.setErrorCode(201);
				responseCommon.setErorrDecreption("SERVICE ERROR");
				responseCommon.setResult(false);
			}
		} catch (Exception e) {
			responseCommon.setErrorCode(201);
			responseCommon.setErorrDecreption("SERVICE ERROR");
			responseCommon.setResult(false);
		}
		
		return responseCommon;
	}

}
