package com.tarabol.api.el.controller;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tarabol.api.el.service.DeckService;
import com.tarabol.api.el.service.DeckUserService;
import com.tarabol.api.el.service.UserService;
import com.tarabol.dto.DeckUserDto;
import com.tarabol.response.ResponseCommon;

@Controller
@RequestMapping(value = "/deckUser", produces = "application/json;charset=UTF-8")
public class DeckUserController {

	@Autowired
	private DeckUserService  deckUserService;
	
	@Autowired
	private UserService  userService;
	
	@Autowired
	private DeckService  deckService;

	private ResponseCommon responseCommon;
	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@ResponseBody
	public ResponseCommon addDeckUser(@RequestBody DeckUserDto deckUserDto) {
		responseCommon = new ResponseCommon();
		// validate input data
		if (deckUserDto.getDeckId() > 0 && deckUserDto.getUserId() > 0) {
			// check exist deck and user
			if (!CollectionUtils.isEmpty(userService.findUserByID(deckUserDto.getUserId())) && !CollectionUtils.isEmpty(deckService.findDeckByID(deckUserDto.getDeckId()))) {
				if (deckUserService.saveDeckUser(deckUserDto)) {
					responseCommon.setResult(true);
				} else {
					responseCommon.setErrorCode(201);
					responseCommon.setErorrDecreption("SERVICE ERROR");
					responseCommon.setResult(false);
				}
			} else {
				responseCommon.setErrorCode(103);
				responseCommon.setErorrDecreption("PARENT ID DOES NOT EXIST");
				responseCommon.setResult(false);
			}
		} else {
			responseCommon.setErrorCode(101);
			responseCommon.setErorrDecreption("CAN NOT VALIDATE INPUT DATA");
			responseCommon.setResult(false);
		}
		return responseCommon;
	}
	
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@ResponseBody
	public ResponseCommon deleteDeckUser(@RequestBody DeckUserDto deckUserDto) {
		responseCommon = new ResponseCommon();
		// validate input data
		if (deckUserDto.getDeckUserId() > 0) {
			// check exist deck and user
			if (!CollectionUtils.isEmpty(deckUserService.findDeckUserByUserID(deckUserDto))) {
				if (deckUserService.deleteDeckUser(deckUserDto)) {
					responseCommon.setResult(true);
				} else {
					responseCommon.setErrorCode(201);
					responseCommon.setErorrDecreption("SERVICE ERROR");
					responseCommon.setResult(false);
				}
			} else {
				responseCommon.setErrorCode(102);
				responseCommon.setErorrDecreption("ID DOES NOT EXIST");
				responseCommon.setResult(false);
			}
		} else {
			responseCommon.setErrorCode(101);
			responseCommon.setErorrDecreption("CAN NOT VALIDATE INPUT DATA");
			responseCommon.setResult(false);
		}
		return responseCommon;
	}

}
