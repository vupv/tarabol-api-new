package com.tarabol.api.el.controller;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tarabol.api.el.service.UserService;
import com.tarabol.api.el.util.StringValidator;
import com.tarabol.dto.UserDto;
import com.tarabol.model.User;
import com.tarabol.response.ResponseCommon;

@Controller
@RequestMapping(value = "/user", produces = "application/json;charset=UTF-8")
public class UserController {

	@Autowired
	private UserService userService;

	private ResponseCommon responseCommon;

	@RequestMapping(value = "/findAll", method = RequestMethod.GET)
	@ResponseBody
	public ResponseCommon findAll() {
		responseCommon = new ResponseCommon();
		List<User> result = userService.findUser();
		if (!CollectionUtils.isEmpty(result)) {
			responseCommon.setBodyResponse(result);
			responseCommon.setResult(true);
		} else {
			responseCommon.setErrorCode(101);
			responseCommon.setErorrDecreption("OBJECT NOT FOUND");
			responseCommon.setResult(false);
		}
		return responseCommon;
	}

	@RequestMapping(value = "/findByID", method = RequestMethod.POST)
	@ResponseBody
	public ResponseCommon findByID(@RequestBody UserDto userDto) {
		responseCommon = new ResponseCommon();
		List<User> result = userService.findUserByID(userDto.getUserId());
		if (!CollectionUtils.isEmpty(result)) {
			responseCommon.setBodyResponse(result);
			responseCommon.setResult(true);
		} else {
			responseCommon.setErrorCode(101);
			responseCommon.setErorrDecreption("OBJECT NOT FOUND");
			responseCommon.setResult(false);
		}
		return responseCommon;
	}

	@RequestMapping(value = "/signUp", method = RequestMethod.POST)
	@ResponseBody
	public ResponseCommon signUp(@RequestBody UserDto userDto) {
		responseCommon = new ResponseCommon();
		if (userService.checkExistAccount(userDto)) {
			responseCommon.setErrorCode(104);
			responseCommon.setErorrDecreption("USERNAME_EMAIL_ALREADY_EXIST");
			responseCommon.setResult(false);
			return responseCommon;
		}

		if (userDto.getEmail().isEmpty() && userDto.getUsername().isEmpty()) {
			responseCommon.setErrorCode(101);
			responseCommon.setErorrDecreption("CAN_NOT_VALIDATE_INPUT_DATA");
			responseCommon.setResult(false);
		} else {
			// validate email
			if (!userDto.getEmail().isEmpty() && userDto.getEmail() != null) {
				if (StringValidator.eMailValidator(userDto.getEmail())) {
					List<UserDto> user = userService.login(userDto);
					if (!CollectionUtils.isEmpty(user)) {
						responseCommon.setBodyResponse(user);
						responseCommon.setResult(true);
					} else {
						// sai pass or email
						responseCommon.setErrorCode(105);
						responseCommon.setErorrDecreption("USERNAME_PASSWORD_WRONG");
						responseCommon.setResult(false);
					}
				} else {
					// sai dinh dang email
					responseCommon.setErrorCode(101);
					responseCommon.setErorrDecreption("CAN_NOT_VALIDATE_INPUT_DATA");
					responseCommon.setResult(false);
				}
			} else {

				if (StringValidator.fieldNotNull(userDto.getUsername())) {
					List<UserDto> user = userService.login(userDto);
					if (!CollectionUtils.isEmpty(user)) {
						responseCommon.setBodyResponse(user);
						responseCommon.setResult(true);
					} else {
						responseCommon.setErrorCode(105);
						responseCommon.setErorrDecreption("USERNAME_PASSWORD_WRONG");
						responseCommon.setResult(false);

					}
				} else {
					responseCommon.setErrorCode(101);
					responseCommon.setErorrDecreption("CAN_NOT_VALIDATE_INPUT_DATA");
					responseCommon.setResult(false);
				}
			}
		}

		return responseCommon;
	}

	@RequestMapping(value = "/signIn", method = RequestMethod.POST)
	@ResponseBody
	public ResponseCommon signIn(@RequestBody UserDto userDto) {
		responseCommon = new ResponseCommon();
		if (userDto.getEmail().isEmpty() && userDto.getUsername().isEmpty()) {
			responseCommon.setErrorCode(101);
			responseCommon.setErorrDecreption("CAN_NOT_VALIDATE_INPUT_DATA");
			responseCommon.setResult(false);
		} else {
			// validate email
			if (!userDto.getEmail().isEmpty() && userDto.getEmail() != null) {
				if (StringValidator.eMailValidator(userDto.getEmail())) {
					List<UserDto> user = userService.login(userDto);
					if (!CollectionUtils.isEmpty(user)) {
						responseCommon.setBodyResponse(user);
						responseCommon.setResult(true);
					} else {
						// sai pass or email
						//
						responseCommon.setErrorCode(105);
						responseCommon.setErorrDecreption("USERNAME_PASSWORD_WRONG");
						responseCommon.setResult(false);

					}
				} else {
					// sai dinh dang email
					responseCommon.setErrorCode(101);
					responseCommon.setErorrDecreption("CAN_NOT_VALIDATE_INPUT_DATA");
					responseCommon.setResult(false);
				}
			} else {
				if (StringValidator.fieldNotNull(userDto.getUsername())) {
					List<UserDto> user = userService.login(userDto);
					if (!CollectionUtils.isEmpty(user)) {
						responseCommon.setBodyResponse(user);
						responseCommon.setResult(true);
					} else {
						responseCommon.setErrorCode(105);
						responseCommon.setErorrDecreption("USERNAME_PASSWORD_WRONG");
						responseCommon.setResult(false);
						

					}
				} else {
					responseCommon.setErrorCode(101);
					responseCommon.setErorrDecreption("CAN_NOT_VALIDATE_INPUT_DATA");
					responseCommon.setResult(false);
				}
			}
		}

		return responseCommon;
	}
	@RequestMapping(value = "/Update", method = RequestMethod.POST)
	@ResponseBody
	public ResponseCommon Update(@RequestBody UserDto userDto) {
		responseCommon = new ResponseCommon();
		if (userDto.getEmail().isEmpty() && userDto.getUsername().isEmpty()) {
			responseCommon.setErrorCode(101);
			responseCommon.setErorrDecreption("CAN_NOT_VALIDATE_INPUT_DATA");
			responseCommon.setResult(false);
		} else {
			// validate email
			if (!userDto.getEmail().isEmpty() && userDto.getEmail() != null) {
				if (StringValidator.eMailValidator(userDto.getEmail())) {
					List<UserDto> user = userService.login(userDto);
					if (!CollectionUtils.isEmpty(user)) {
						responseCommon.setBodyResponse(user);
						responseCommon.setResult(true);
					} else {
						// sai pass or email
						//
						responseCommon.setErrorCode(105);
						responseCommon.setErorrDecreption("USERNAME_PASSWORD_WRONG");
						responseCommon.setResult(false);

					}
				} else {
					// sai dinh dang email
					responseCommon.setErrorCode(101);
					responseCommon.setErorrDecreption("CAN_NOT_VALIDATE_INPUT_DATA");
					responseCommon.setResult(false);
				}
			} else {
				if (StringValidator.fieldNotNull(userDto.getUsername())) {
					List<UserDto> user = userService.login(userDto);
					if (!CollectionUtils.isEmpty(user)) {
						responseCommon.setBodyResponse(user);
						responseCommon.setResult(true);
					} else {
						responseCommon.setErrorCode(105);
						responseCommon.setErorrDecreption("USERNAME_PASSWORD_WRONG");
						responseCommon.setResult(false);
						

					}
				} else {
					responseCommon.setErrorCode(101);
					responseCommon.setErorrDecreption("CAN_NOT_VALIDATE_INPUT_DATA");
					responseCommon.setResult(false);
				}
			}
		}

		return responseCommon;
	}

}
