package com.tarabol.api.el.controller;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tarabol.api.el.service.CardService;
import com.tarabol.api.el.service.LessonService;
import com.tarabol.dto.CardDto;
import com.tarabol.model.Card;
import com.tarabol.response.ResponseCommon;

@Controller
@RequestMapping(value = "/card", produces = "application/json;charset=UTF-8")
public class CardController {

	@Autowired
	private CardService cardService;

	@Autowired
	private LessonService lessonService;

	private ResponseCommon responseCommon;

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@ResponseBody
	public ResponseCommon saveCard(@RequestBody CardDto cardDto) {
		responseCommon = new ResponseCommon();
		// check operation
		if (cardDto.getCardId() > 0) {
			// update card
			// check exist card
			if (!CollectionUtils.isEmpty(cardService.findCardByID(cardDto))) {
				// check parent id
				if (cardDto.getLessonId() > 0) {
					// parent id not null
					// check exist parent
					if (!CollectionUtils.isEmpty(lessonService.findLessonByID(cardDto.getLessonId()))) {
						// exist parent
						if (cardService.saveCard(cardDto)) {
							responseCommon.setResult(true);
						} else {
							responseCommon.setErrorCode(201);
							responseCommon.setErorrDecreption("SERVICE ERROR");
							responseCommon.setResult(false);
						}
					} else {
						// parent not exist
						responseCommon.setErrorCode(103);
						responseCommon.setErorrDecreption("PARENT ID DOES NOT EXIST");
						responseCommon.setResult(false);
					}
				} else {
					// parent id is null
					responseCommon.setErrorCode(101);
					responseCommon.setErorrDecreption("CAN NOT VALIDATE INPUT DATA");
					responseCommon.setResult(false);
				}
			} else {
				// update id not exist
				responseCommon.setErrorCode(102);
				responseCommon.setErorrDecreption("ID DOES NOT EXIST");
				responseCommon.setResult(false);
			}
		} else {
			// add card
			// check parent id
			if (cardDto.getLessonId() > 0) {
				if (!CollectionUtils.isEmpty(lessonService.findLessonByID(cardDto.getLessonId()))) {
					if (cardService.saveCard(cardDto)) {
						responseCommon.setResult(true);
					} else {
						responseCommon.setErrorCode(201);
						responseCommon.setErorrDecreption("SERVICE ERROR");
						responseCommon.setResult(false);
					}
				} else {
					// parent not exist
					responseCommon.setErrorCode(103);
					responseCommon.setErorrDecreption("PARENT ID DOES NOT EXIST");
					responseCommon.setResult(false);
				}
			} else {
				// parent id is null
				responseCommon.setErrorCode(101);
				responseCommon.setErorrDecreption("CAN NOT VALIDATE INPUT DATA");
				responseCommon.setResult(false);
			}
		}
		return responseCommon;
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@ResponseBody
	public ResponseCommon deleteCard(@RequestBody CardDto cardDto) {
		responseCommon = new ResponseCommon();
		// check id
		if (cardDto.getCardId() > 0) {
			if (!CollectionUtils.isEmpty(cardService.findCardByID(cardDto))) {
				if (cardService.deleteCard(cardDto)) {
					responseCommon.setResult(true);
				} else {
					responseCommon.setErrorCode(201);
					responseCommon.setErorrDecreption("SERVICE ERROR");
					responseCommon.setResult(false);
				}
			} else {
				responseCommon.setErrorCode(102);
				responseCommon.setErorrDecreption("ID DOES NOT EXIST");
				responseCommon.setResult(false);
			}
		} else {
			responseCommon.setErrorCode(102);
			responseCommon.setErorrDecreption("ID DOES NOT EXIST");
			responseCommon.setResult(false);
		}
		return responseCommon;
	}

	@RequestMapping(value = "/findCardByID", method = RequestMethod.POST)
	@ResponseBody
	public ResponseCommon findCardByID(@RequestBody CardDto cardDto) {
		responseCommon = new ResponseCommon();
		// check id
		if (cardDto.getCardId() > 0) {
			List<Card> result = cardService.findCardByID(cardDto);
			if (!CollectionUtils.isEmpty(result)) {
				responseCommon.setBodyResponse(result);
				responseCommon.setResult(true);
			} else {
				responseCommon.setErrorCode(101);
				responseCommon.setErorrDecreption("OBJECT NOT FOUND");
				responseCommon.setResult(false);
			}
		} else {
			responseCommon.setErrorCode(102);
			responseCommon.setErorrDecreption("ID DOES NOT EXIST");
			responseCommon.setResult(false);
		}
		return responseCommon;
	}

	@RequestMapping(value = "/findCardByLessonID", method = RequestMethod.POST)
	@ResponseBody
	public ResponseCommon findCardByLessonID(@RequestBody CardDto cardDto) {
		responseCommon = new ResponseCommon();
		// check id
		if (cardDto.getLessonId() > 0) {
			List<Card> result = cardService.findCardByLessonID(cardDto.getLessonId());
			if (!CollectionUtils.isEmpty(result)) {
				responseCommon.setBodyResponse(result);
				responseCommon.setResult(true);
			} else {
				responseCommon.setErrorCode(101);
				responseCommon.setErorrDecreption("OBJECT NOT FOUND");
				responseCommon.setResult(false);
			}
		} else {
			responseCommon.setErrorCode(102);
			responseCommon.setErorrDecreption("ID DOES NOT EXIST");
			responseCommon.setResult(false);
		}
		return responseCommon;
	}

}
