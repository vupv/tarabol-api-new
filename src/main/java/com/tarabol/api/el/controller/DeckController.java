package com.tarabol.api.el.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Base64Utils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tarabol.api.el.configuration.AppConfig;
import com.tarabol.api.el.service.CategoryService;
import com.tarabol.api.el.service.DeckService;
import com.tarabol.api.el.service.FileService;
import com.tarabol.api.el.service.UserService;
import com.tarabol.dto.DeckDto;
import com.tarabol.dto.UserDto;
import com.tarabol.model.Deck;
import com.tarabol.request.DeckRequest;
import com.tarabol.response.ResponseCommon;

@Controller
@RequestMapping(value = "/deck", produces = "application/json;charset=UTF-8")
public class DeckController {

	@Autowired
	private DeckService deckService;

	@Autowired
	private UserService userService;

	@Autowired
	private CategoryService categoryService;

	@Autowired
	private FileService fileService;

	private ResponseCommon responseCommon;

	@RequestMapping(value = "/findAll", method = RequestMethod.GET)
	@ResponseBody
	public ResponseCommon findAll() {
		responseCommon = new ResponseCommon();
		List<Deck> result = deckService.findDeck();
		if (!CollectionUtils.isEmpty(result)) {
			responseCommon.setBodyResponse(result);
			responseCommon.setResult(true);
		} else {
			responseCommon.setErrorCode(101);
			responseCommon.setErorrDecreption("OBJECT NOT FOUND");
			responseCommon.setResult(false);
		}
		return responseCommon;
	}

	@RequestMapping(value = "/findByUser", method = RequestMethod.POST)
	@ResponseBody
	public ResponseCommon findByUser(@RequestBody UserDto userDto) {
		responseCommon = new ResponseCommon();
		List<Deck> result = deckService.findDeckByUser(userDto);
		if (userDto.getUserId() > 0 && userDto.getMax() - userDto.getFirst() < Integer.valueOf(AppConfig.PROPERTY_NAME_MAX_RECORD)) {
			if (!CollectionUtils.isEmpty(result)) {
				responseCommon.setBodyResponse(result);
				responseCommon.setResult(true);
			} else {
				responseCommon.setErrorCode(101);
				responseCommon.setErorrDecreption("OBJECT NOT FOUND");
				responseCommon.setResult(false);
			}
		} else {
			responseCommon.setErrorCode(102);
			responseCommon.setErorrDecreption("ID DOES NOT EXIST");
			responseCommon.setResult(false);
		}
		return responseCommon;
	}

	@RequestMapping(value = "/findByID", method = RequestMethod.POST)
	@ResponseBody
	public ResponseCommon findByID(@RequestBody DeckDto deckDto) {
		responseCommon = new ResponseCommon();
		List<Deck> result = deckService.findDeckByID(deckDto.getDeckId());
		if (!CollectionUtils.isEmpty(result)) {
			responseCommon.setBodyResponse(result);
			responseCommon.setResult(true);
		} else {
			responseCommon.setErrorCode(101);
			responseCommon.setErorrDecreption("OBJECT NOT FOUND");
			responseCommon.setResult(false);
		}
		return responseCommon;
	}

	@RequestMapping(value = "/findTop", method = RequestMethod.POST)
	@ResponseBody
	public ResponseCommon findDeckTop(@RequestBody DeckDto deckDto) {
		responseCommon = new ResponseCommon();
		List<Deck> result = deckService.findDeckTop(deckDto.getMax());
		if (!CollectionUtils.isEmpty(result)) {
			responseCommon.setBodyResponse(result);
			responseCommon.setResult(true);
		} else {
			responseCommon.setErrorCode(101);
			responseCommon.setErorrDecreption("OBJECT NOT FOUND");
			responseCommon.setResult(false);
		}
		return responseCommon;
	}

	// @RequestMapping(value = "/save", method = RequestMethod.POST)
	// @ResponseBody
	public ResponseCommon addDeck(@RequestBody DeckDto deckDto) {
		responseCommon = new ResponseCommon();
		// check user ID
		if (deckDto.getDeckId() > 0) {
			if (deckDto.getUserId() > 0) {
				// check exist user
				if (!CollectionUtils.isEmpty(userService.findUserByID(deckDto.getUserId()))) {
					if (deckService.saveDeck(deckDto)) {
						responseCommon.setResult(true);
					} else {
						responseCommon.setErrorCode(201);
						responseCommon.setErorrDecreption("SERVICE ERROR");
						responseCommon.setResult(false);
					}
				} else {
					responseCommon.setErrorCode(103);
					responseCommon.setErorrDecreption("PARENT ID DOES NOT EXIST");
					responseCommon.setResult(false);
				}
			} else {
				responseCommon.setErrorCode(101);
				responseCommon.setErorrDecreption("CAN NOT VALIDATE INPUT DATA");
				responseCommon.setResult(false);
			}
		} else {
			responseCommon.setErrorCode(101);
			responseCommon.setErorrDecreption("CAN NOT VALIDATE INPUT DATA");
			responseCommon.setResult(false);
		}
		return responseCommon;
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@ResponseBody
	public ResponseCommon addDeck(DeckRequest deck, byte[] icon, String iconFileName) {
		responseCommon = new ResponseCommon();

		// call service for save file
		String fileSaved = fileService.saveFileToStore(Base64Utils.decode(icon), iconFileName);
		if (fileSaved != null) {
			// call service save deck
			Date current = new Date();
			DeckDto deckDTO = DeckDto.convertDeckToDTO(deck);
			deckDTO.setCreateTime(current);
			deckDTO.setIcon(fileSaved);
			deckDTO.setUpdateTime(current);
			if (deckService.saveDeck(deckDTO)) {
				responseCommon.setResult(true);
				responseCommon.setBodyResponse(Arrays.asList(deckDTO.getDeckId()));
			} else {
				responseCommon.setErrorCode(201);
				responseCommon.setErorrDecreption("SERVICE ERROR");
				responseCommon.setResult(false);
			}
		} else {
			responseCommon.setErrorCode(101);
			responseCommon.setErorrDecreption("CAN NOT WRITE FILE");
			responseCommon.setResult(false);
		}
		return responseCommon;
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@ResponseBody
	public ResponseCommon deleteDeck(@RequestBody DeckDto deckDto) {
		responseCommon = new ResponseCommon();
		// check user ID
		if (deckDto.getDeckId() > 0) {
			// check exist user
			if (!CollectionUtils.isEmpty(deckService.findDeckByID(deckDto.getDeckId()))) {
				if (deckService.saveDeck(deckDto)) {
					responseCommon.setResult(true);
				} else {
					responseCommon.setErrorCode(201);
					responseCommon.setErorrDecreption("SERVICE ERROR");
					responseCommon.setResult(false);
				}
			} else {
				responseCommon.setErrorCode(102);
				responseCommon.setErorrDecreption("ID DOES NOT EXIST");
				responseCommon.setResult(false);
			}
		} else {
			responseCommon.setErrorCode(101);
			responseCommon.setErorrDecreption("CAN NOT VALIDATE INPUT DATA");
			responseCommon.setResult(false);
		}
		return responseCommon;
	}
	
	@RequestMapping(value = "/findBestNew/{quantity}", method = RequestMethod.GET)
	@ResponseBody
	public List<DeckDto> findBestNew(@PathVariable("quantity") int quantity) {

		List<DeckDto> result = new ArrayList<DeckDto>();
		if (quantity < 1) {
			return result;
		}

		result = deckService.findBestNew(1, quantity);
		if (CollectionUtils.isEmpty(result)) {
			return result;
		}
		return result;
	}
	
	@RequestMapping(value = "/findPopular/{quantity}", method = RequestMethod.GET)
	@ResponseBody
	public List<DeckDto> findPopular(@PathVariable("quantity") int quantity) {

		List<DeckDto> result = new ArrayList<DeckDto>();
		if (quantity <= 0) {
			return result;
		}

		result = deckService.findPopular(1, quantity);
		if (CollectionUtils.isEmpty(result)) {
			return result;
		}
		return result;
	}
	

	@RequestMapping(value = "/countFindAll", method = RequestMethod.GET)
	@ResponseBody
	public long countFindAll() {

		return deckService.countFindAll();
	}
	
	@RequestMapping(value = "/findByCategory/{categoryId}/{page}/{quantity}", method = RequestMethod.GET)
	@ResponseBody
	public List<DeckDto> findByCategory(@PathVariable("categoryId") long categoryId,
			@PathVariable("page") int page, @PathVariable("quantity") int quantity) {

		List<DeckDto> result = new ArrayList<DeckDto>();
		if (page < 1 || quantity < 1) {
			return result;
		}

		result = deckService.find("", categoryId, page, quantity);
		if (CollectionUtils.isEmpty(result)) {
			return result;
		}
		return result;
	}
	
	@RequestMapping(value = "/countFindByCategory/{categoryId}", method = RequestMethod.GET)
	@ResponseBody
	public long countFind(@PathVariable("categoryId") long categoryId) {
		
		return deckService.countFind("", categoryId);
	}

	
	@RequestMapping(value = "/findBestNewByPage/{page}/{quantity}", method = RequestMethod.GET)
	@ResponseBody
	public List<DeckDto> findBestNewByPage(@PathVariable("page") int page, @PathVariable("quantity") int quantity) {

		List<DeckDto> result = new ArrayList<DeckDto>();
		if (page < 1 || quantity < 1) {
			return result;
		}

		result = deckService.findBestNew(page, quantity);
		if (CollectionUtils.isEmpty(result)) {
			return result;
		}
		return result;
	}

	@RequestMapping(value = "/findPopularByPage/{page}/{quantity}", method = RequestMethod.GET)
	@ResponseBody
	public List<DeckDto> findPopularByPage(@PathVariable("page") int page, @PathVariable("quantity") int quantity) {

		List<DeckDto> result = new ArrayList<DeckDto>();
		if (page < 1 || quantity < 1) {
			return result;
		}

		result = deckService.findPopular(page, quantity);
		if (CollectionUtils.isEmpty(result)) {
			return result;
		}
		return result;
	}
	
	
	
	
	
}
