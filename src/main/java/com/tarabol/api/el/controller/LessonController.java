package com.tarabol.api.el.controller;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tarabol.api.el.service.LessonService;
import com.tarabol.dto.LessonDto;
import com.tarabol.model.Lesson;
import com.tarabol.request.LessonRequest;
import com.tarabol.response.ResponseCommon;

@Controller
@RequestMapping(value = "/lesson", produces = "application/json;charset=UTF-8")
public class LessonController {

	@Autowired
	private LessonService lessonService;

	private ResponseCommon responseCommon;

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@ResponseBody
	public ResponseCommon addLesson(@RequestBody LessonRequest lesson) {
		responseCommon = new ResponseCommon();
		// check deckID
		LessonDto dto = lesson.convertToDTO();
		if (lessonService.saveLesson(dto)) {
			responseCommon.setResult(true);
			responseCommon.setBodyResponse(Arrays.asList(dto));
		} else {
			responseCommon.setErrorCode(201);
			responseCommon.setErorrDecreption("SERVICE ERROR");
			responseCommon.setResult(false);
		}
		return responseCommon;
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@ResponseBody
	public ResponseCommon deleteLesson(@RequestBody LessonDto lessonDto) {
		responseCommon = new ResponseCommon();
		// check deckID
		if (lessonDto.getLessonId() > 0) {
			if (!CollectionUtils.isEmpty(lessonService.findLessonByID(lessonDto.getLessonId()))) {
				if (lessonService.deleteLesson(lessonDto)) {
					responseCommon.setResult(true);
				} else {
					responseCommon.setErrorCode(201);
					responseCommon.setErorrDecreption("SERVICE ERROR");
					responseCommon.setResult(false);
				}
			} else {
				responseCommon.setErrorCode(102);
				responseCommon.setErorrDecreption("ID DOES NOT EXIST");
				responseCommon.setResult(false);
			}
		} else {
			responseCommon.setErrorCode(101);
			responseCommon.setErorrDecreption("CAN NOT VALIDATE INPUT DATA");
			responseCommon.setResult(false);
		}
		return responseCommon;
	}

	@RequestMapping(value = "/getByDeck", method = RequestMethod.GET)
	@ResponseBody
	public ResponseCommon findLessonByDeckID(@RequestParam long deckId) {
		ResponseCommon responseCommon = new ResponseCommon();
		// check id
		List<Lesson> result = lessonService.findLessonByDeckID(deckId);
		if (!CollectionUtils.isEmpty(result)) {
			responseCommon.setBodyResponse(result);
			responseCommon.setResult(true);
		} else {
			responseCommon.setErrorCode(101);
			responseCommon.setErorrDecreption("OBJECT NOT FOUND");
			responseCommon.setResult(false);
		}
		return responseCommon;
	}

}
