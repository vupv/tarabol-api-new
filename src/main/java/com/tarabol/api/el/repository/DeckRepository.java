package com.tarabol.api.el.repository;

import org.springframework.data.repository.CrudRepository;

import com.tarabol.model.Deck;

public interface DeckRepository extends CrudRepository<Deck, Long>, DeckRepositoryCustom {
}
