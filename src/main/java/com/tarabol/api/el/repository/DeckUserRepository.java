package com.tarabol.api.el.repository;

import org.springframework.data.repository.CrudRepository;

import com.tarabol.model.DeckUser;

public interface DeckUserRepository extends CrudRepository<DeckUser, Long>, DeckUserRepositoryCustom {

}
