package com.tarabol.api.el.repository;

import java.util.List;

import com.tarabol.dto.LessonDto;
import com.tarabol.model.Lesson;


public interface LessonRepositoryCustom {
	public boolean saveLesson(LessonDto lessonDto);
	public boolean deleteLesson(LessonDto lessonDto);
	public List<Lesson> findLessonByID(long lessonID);
	public List<Lesson> findLessonByDeckID(long deckID);
}
