package com.tarabol.api.el.repository;

import org.springframework.data.repository.CrudRepository;

import com.tarabol.model.Lesson;

public interface LessonRepository extends CrudRepository<Lesson, Long>, LessonRepositoryCustom {
}