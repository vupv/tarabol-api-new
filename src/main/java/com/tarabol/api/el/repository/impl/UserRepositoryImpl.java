package com.tarabol.api.el.repository.impl;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.tarabol.api.el.configuration.AppConfig;
import com.tarabol.api.el.repository.UserRepositoryCustom;
import com.tarabol.api.el.util.MyNumberUtil;
import com.tarabol.dto.UserDto;
import com.tarabol.model.User;

//loi xoa model
@Repository
public class UserRepositoryImpl implements UserRepositoryCustom {

	@PersistenceContext
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	@Override
	public List<User> findUser(){
		Query query = entityManager.createNativeQuery(AppConfig.SQL_GET_USER, User.class);
//		Query query = entityManager.createNamedQuery("User.findAll");
//		List a = query.getResultList();
		return query.getResultList();
//		return a;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<User> findUserByID(long userID) {
		Query query = entityManager.createNativeQuery(AppConfig.SQL_GET_USER_BY_ID, User.class);
		query.setParameter("user_id", userID);
		return query.getResultList();
	}
	
	@Override
	public List<UserDto> login(UserDto userDto) {
		Query query = entityManager.createNativeQuery(AppConfig.SQL_LOGIN, User.class); // createNativeQuery(string
		// sql,
		// parser
		// class)
		query.setParameter("username", userDto.getUsername());
		query.setParameter("password", MyNumberUtil.encryptMD5(userDto.getPassword()));
		List<User> users = query.getResultList();
		List<UserDto> rs = new ArrayList<>();
		for (User user : users) {
			UserDto dto = new UserDto();
			dto.setUserId(user.getUserId());
			rs.add(dto);
		}
		return rs;
	}

	@Override
	public boolean insertUser(UserDto userDto) {
		// TODO Auto-generated method stub
		String sql = "INSERT INTO USER(USER_ID, NAME, EMAIL, AVATAR, LEARN_CONFIG_ID, PASSWORD, USERNAME, STATUS, CREATE_TIME, UPDATE_TIME, BIO )"
				+ "VALUES ( USER_ID_SEQ.NEXTVAL, :name, :email, :avarta, :learnConfigId, :password, :username, 1, to_date(sysdate, 'dd/mm/yyyy'),  to_date(sysdate, 'dd/mm/yyyy'),:bio )";
		Query query = entityManager.createNativeQuery(sql);
		query.setParameter("username", userDto.getUsername());
		query.setParameter("password", MyNumberUtil.encryptMD5(userDto.getPassword()));
		query.setParameter("email", userDto.getEmail());

		return query.executeUpdate() > 0;
	}

	@Override
	public boolean checkExistAccount(UserDto userDto) {
		String sql = "SELECT COUNT(USER_ID) FROM USER WHERE NAME=:name OR EMAIL=:email";
		Query query = entityManager.createNativeQuery(sql);
		query.setParameter("username", userDto.getUsername());
		query.setParameter("email", userDto.getEmail());

		return ((BigInteger) query.getSingleResult()).longValue() > 0;

	}

	@Override
	public boolean checkAuth(long userId, String userKey) {
		String sql = "SELECT COUNT(NTFY_USER_ID) FROM NTFY_USER WHERE NTFY_USER_ID=:userId AND USER_KEY=:userKey";
		Query query = entityManager.createNativeQuery(sql);
		query.setParameter("userId", userId);
		query.setParameter("userKey", userKey);

		return ((BigInteger) query.getSingleResult()).longValue() > 0;

	}

	@Override
	public boolean Update(UserDto userDto, long UserId) {

		String sql =  AppConfig.SQL_DELETE_USER;
		Query query = entityManager.createNativeQuery(sql);
		return query.executeUpdate() > 0;
	}
}
