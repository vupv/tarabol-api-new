package com.tarabol.api.el.repository.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.tarabol.api.el.configuration.AppConfig;
import com.tarabol.api.el.repository.DeckUserRepositoryCustom;
import com.tarabol.dto.DeckUserDto;
import com.tarabol.model.DeckUser;

@Repository
public class DeckUserRepositoryImpl implements DeckUserRepositoryCustom {
	
	@PersistenceContext
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	@Override
	public List<DeckUser> findDeckUserByUserID(long userID) {
		Query query = entityManager.createNativeQuery(AppConfig.SQL_GET_DECKUSER_BY_USER_ID);
		query.setParameter("user_id", userID);
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DeckUser> findDeckByID(DeckUserDto deckUserDto) {
		Query query = entityManager.createNativeQuery(AppConfig.SQL_GET_DECKUSER_BY_ID);
		query.setParameter("deck_user_id", deckUserDto.getDeckUserId());
		return query.getResultList();
	}

	@Override
	public boolean saveDeckUser(DeckUserDto deckUserDto) {
		String sql = deckUserDto.getDeckUserId() <= 0 ? AppConfig.SQL_ADD_DECKUSER : AppConfig.SQL_UPDATE_DECKUSER;
		Query query = entityManager.createNativeQuery(sql);
		if(deckUserDto.getDeckUserId() <= 0){
		query.setParameter("user_id", deckUserDto.getUserId());
		query.setParameter("deck_id", deckUserDto.getDeckId());
		} else {
			query.setParameter("user_id", deckUserDto.getUserId());
			query.setParameter("deck_id", deckUserDto.getDeckId());
			query.setParameter("deck_user_id", deckUserDto.getDeckUserId());
		}
		return query.executeUpdate() > 0;
	}

	@Override
	public boolean deleteDeckUser(DeckUserDto deckUserDto) {
		Query query = entityManager.createNativeQuery(AppConfig.SQL_DELETE_DECKUSER);
		query.setParameter("deck_user_id", deckUserDto.getDeckUserId());
		return query.executeUpdate() > 0;
	}

	
}
