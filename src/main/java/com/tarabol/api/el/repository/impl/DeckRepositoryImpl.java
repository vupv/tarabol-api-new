package com.tarabol.api.el.repository.impl;

import java.math.BigInteger;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.tarabol.api.el.configuration.AppConfig;
import com.tarabol.api.el.repository.DeckRepositoryCustom;
import com.tarabol.dto.DeckDto;
import com.tarabol.model.Deck;

@Repository
public class DeckRepositoryImpl implements DeckRepositoryCustom {

	@PersistenceContext
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	@Override
	public List<Deck> findDeck() {
		// Query query = entityManager.createNamedQuery("Deck.findAll");
		Query query = entityManager.createNativeQuery(AppConfig.SQL_GET_DECK, Deck.class);
		return query.getResultList();
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public List<Deck> findDeckByMultiID(List deckID) {
		// Query query = entityManager.createNamedQuery("Deck.findByMultiID");
		Query query = entityManager.createNativeQuery(AppConfig.SQL_GET_DECK_BY_MULTI_ID, Deck.class);
		query.setParameter("deckid", deckID);
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Deck> findDeckTop(int top) {
		Query query = entityManager.createNativeQuery(AppConfig.SQL_GET_DECK_BY_ID, Deck.class);
		query.setFirstResult(0);
		query.setMaxResults(top);
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Deck> findDeckByID(long deckID) {
		Query query = entityManager.createNativeQuery(AppConfig.SQL_GET_DECK_BY_ID, Deck.class);
		query.setParameter("deck_id", deckID);
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Deck> findDeckByUser(long userID, int first, int max) {
		Query query = entityManager.createNativeQuery(AppConfig.SQL_GET_DECK_BY_USER, Deck.class);
		query.setParameter("user_id", userID);
		query.setFirstResult(first);
		query.setMaxResults(max);
		List<Deck> decks = query.getResultList();
		return decks;
	}

	@Override
	public boolean saveDeck(DeckDto deckDto) {
		String sql = deckDto.getDeckId() <= 0 ? AppConfig.SQL_ADD_DECK : AppConfig.SQL_UPDATE_DECK;
		Query query = entityManager.createNativeQuery(sql);
		if (deckDto.getDeckId() <= 0) {
			query.setParameter("name", deckDto.getName());
			query.setParameter("categories_id", deckDto.getCategoriesId());
			query.setParameter("icon", deckDto.getIcon());
			query.setParameter("price", deckDto.getPrice());
			query.setParameter("total_download", deckDto.getTotalDownload());
			query.setParameter("evaluate", deckDto.getEvaluate());
			query.setParameter("user_id", deckDto.getUserId());
			query.setParameter("tags", deckDto.getTags());
			query.setParameter("description", deckDto.getDescription());
		} else {
			query.setParameter("name", deckDto.getName());
			query.setParameter("categories_id", deckDto.getCategoriesId());
			query.setParameter("icon", deckDto.getIcon());
			query.setParameter("price", deckDto.getPrice());
			query.setParameter("total_download", deckDto.getTotalDownload());
			query.setParameter("evaluate", deckDto.getEvaluate());
			query.setParameter("user_id", deckDto.getUserId());
			query.setParameter("tags", deckDto.getTags());
			query.setParameter("description", deckDto.getDescription());
			query.setParameter("deck_id", deckDto.getDeckId());
		}
		boolean rs = query.executeUpdate() > 0;
		if(rs){
			BigInteger lastId = (BigInteger) ((Session)entityManager.getDelegate()).createSQLQuery("SELECT LAST_INSERT_ID()") .uniqueResult();
			deckDto.setDeckId(lastId.intValue());
		}
		return rs;
	}

	@Override
	public boolean deleteDeck(DeckDto deckDto) {
		Query query = entityManager.createNativeQuery(AppConfig.SQL_DELETE_DECK);
		query.setParameter("deck_id", deckDto.getDeckId());
		return query.executeUpdate() > 0;
	}

	@Override
	public Long countFindAll() {
		
		Query query = entityManager.createNativeQuery(AppConfig.SQL_COUNT_FIND_ALL_DECK);
		BigInteger result = (BigInteger) query.getSingleResult();
		return result.longValue();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DeckDto> findBestNew(int page, int quantity) {
		
		Query query = entityManager.createNamedQuery("Deck.findAll");
		query.setFirstResult((page - 1) * quantity);
		query.setMaxResults(quantity);
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DeckDto> findPopular(int page, int quantity) {
		
		Query query = entityManager.createNamedQuery("Deck.findAll");
		query.setFirstResult((page - 1) * quantity);
		query.setMaxResults(quantity);
		return query.getResultList();
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<DeckDto> find(String freeword, long categoryId, int page, int quantity) {
		
		StringBuilder sql = new StringBuilder("SELECT new com.tarabol.api.el.dto.TopicDto(t.id, t.createDate, t.description, t.downs, t.evaluate, t.image, t.modifyDate, t.name, t.price, "
				+ "t.category.name AS categoryName,"
				+ "t.user.name AS userName) "
				+ "FROM Topic t JOIN t.category c JOIN t.user u "
				+ "WHERE t.delFlg=0 AND c.delFlg=0 AND u.delFlg=0 ");
		if (freeword != null && !freeword.trim().isEmpty()) {
			sql.append("AND UPPER(t.name) LIKE :freeword ");
		}
		
		if (categoryId > 0) {
			sql.append("AND t.category.id = :categoryId ");
		}
		sql.append("ORDER BY t.createDate DESC, t.evaluate DESC");
		
		Query query = entityManager.createQuery(sql.toString());
		if (freeword != null && !freeword.trim().isEmpty()) {
			query.setParameter("freeword", "%" + freeword.toUpperCase() + "%");
		}
		
		if (categoryId > 0) {
			query.setParameter("categoryId", categoryId);
		}
		
		query.setFirstResult((page - 1) * quantity);
		query.setMaxResults(quantity);
		return query.getResultList();
	}


	@Override
	public long countFind(String freeword, long categoryId) {
		
		StringBuilder sql = new StringBuilder("SELECT COUNT(t.id) "
				+ "FROM Deck t JOIN t.category c JOIN t.user u "
				+ "WHERE t.delFlg=0 AND c.delFlg=0 AND u.delFlg=0 ");
		if (freeword != null && !freeword.trim().isEmpty()) {
			sql.append("AND UPPER(t.name) LIKE :freeword ");
		}
		
		if (categoryId > 0) {
			sql.append("AND t.category.id = :categoryId ");
		}
		sql.append("ORDER BY t.createDate DESC, t.evaluate DESC");
		
		Query query = entityManager.createQuery(sql.toString());
		if (freeword != null && !freeword.trim().isEmpty()) {
			query.setParameter("freeword", "%" + freeword.toUpperCase() + "%");
		}
		
		if (categoryId > 0) {
			query.setParameter("categoryId", categoryId);
		}
		return (long)query.getSingleResult();
	}


}
