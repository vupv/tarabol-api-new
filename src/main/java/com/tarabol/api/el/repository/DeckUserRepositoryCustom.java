package com.tarabol.api.el.repository;

import java.util.List;

import com.tarabol.dto.DeckUserDto;
import com.tarabol.model.DeckUser;

public interface DeckUserRepositoryCustom {
	public List<DeckUser> findDeckUserByUserID(long userID);
	public List<DeckUser> findDeckByID(DeckUserDto deckUserDto);
	public boolean saveDeckUser(DeckUserDto deckUserDto);
	public boolean deleteDeckUser(DeckUserDto deckUserDto);
	
}
