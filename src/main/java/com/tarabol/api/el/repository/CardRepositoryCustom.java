package com.tarabol.api.el.repository;

import java.util.List;

import com.tarabol.dto.CardDto;
import com.tarabol.model.Card;


public interface CardRepositoryCustom {
	public List<Card> findCardByID(CardDto cardDto);
	public List<Card> findCardByLessonID(long lessonID);
	public boolean saveCard(CardDto cardDto);
	public boolean deleteCard(CardDto cardDto);
}
