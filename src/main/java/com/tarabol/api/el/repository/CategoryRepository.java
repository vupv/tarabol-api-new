package com.tarabol.api.el.repository;

import org.springframework.data.repository.CrudRepository;

import com.tarabol.model.Category;

public interface CategoryRepository extends CrudRepository<Category, Long>, CategoryRepositoryCustom {
}