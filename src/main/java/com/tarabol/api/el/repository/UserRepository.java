package com.tarabol.api.el.repository;

import org.springframework.data.repository.CrudRepository;

import com.tarabol.model.User;

public interface UserRepository extends CrudRepository<User, Long>, UserRepositoryCustom {

}
