package com.tarabol.api.el.repository;

import java.util.List;

import com.tarabol.dto.DeckDto;
import com.tarabol.model.Deck;

public interface DeckRepositoryCustom {
	public List<Deck> findDeck();

	@SuppressWarnings("rawtypes")
	public List<Deck> findDeckByMultiID(List deckID);

	public List<Deck> findDeckTop(int top);

	public List<Deck> findDeckByUser(long userID, int first, int max);

	public List<Deck> findDeckByID(long deckID);

	public boolean saveDeck(DeckDto deckDto);

	public boolean deleteDeck(DeckDto deckDto);
	
    public Long countFindAll();
	
	public List<DeckDto> findBestNew(int page, int quantity);

	public List<DeckDto> findPopular(int page, int quantity);
	
	public List<DeckDto> find(String freeword, long categoryId, int page, int quantity);
	public long countFind(String freeword, long categoryId);
}
