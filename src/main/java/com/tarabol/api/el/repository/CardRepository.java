package com.tarabol.api.el.repository;

import org.springframework.data.repository.CrudRepository;

import com.tarabol.model.Card;

public interface CardRepository extends CrudRepository<Card, Long>, CardRepositoryCustom {
}