package com.tarabol.api.el.repository.impl;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.springframework.stereotype.Repository;

import com.tarabol.api.el.repository.FileRepository;

@Repository
public class FileRepositoryImpl implements FileRepository {

	//private static final String FOLDER_STORED = "/Wordspace/VuPV4/Tarabol/Data";
	private static final String FOLDER_STORED = "/home/tarabol/data";

	@Override
	public String saveFileToStore(byte[] datas, String fileName) {
		String result;
		String randomFileName = new StringBuffer(UUID.randomUUID().toString()).append(".").append(FilenameUtils.getExtension(fileName)).toString();
		String urlSave = new StringBuilder(FOLDER_STORED).append("/").append(randomFileName).toString();
		try {
			FileUtils.writeByteArrayToFile(new File(urlSave), datas);
			result = randomFileName;
		} catch (IOException e) {
			result = null;
			e.printStackTrace();
		}
		return result;
	}

}
