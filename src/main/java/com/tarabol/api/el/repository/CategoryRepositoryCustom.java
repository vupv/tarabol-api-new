package com.tarabol.api.el.repository;

import java.util.List;

import com.tarabol.dto.CategoryDto;
import com.tarabol.model.Category;

public interface CategoryRepositoryCustom {
	
	public List<Category> findCategory();
	public boolean addCategory(CategoryDto categoryDto);
	public boolean editCategory(CategoryDto categoryDto);
	public boolean deleteCategory(CategoryDto categoryDto);
}
