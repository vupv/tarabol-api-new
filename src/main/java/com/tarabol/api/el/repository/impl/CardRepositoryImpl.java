package com.tarabol.api.el.repository.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestBody;

import com.tarabol.api.el.configuration.AppConfig;
import com.tarabol.api.el.repository.CardRepositoryCustom;
import com.tarabol.dto.CardDto;
import com.tarabol.model.Card;

@Repository
public class CardRepositoryImpl implements CardRepositoryCustom {
	
	@PersistenceContext
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	@Override
	public List<Card> findCardByID(CardDto cardDto) {
		Query query = entityManager.createNativeQuery(AppConfig.SQL_GET_CARD_BY_ID);
		query.setParameter("card_id", cardDto.getCardId());
		return query.getResultList();
	}

	@Override
	public boolean saveCard(@RequestBody CardDto cardDto) {
		String sql = cardDto.getCardId() <= 0 ? AppConfig.SQL_ADD_CARD : AppConfig.SQL_DELETE_CARD;
		Query query = entityManager.createNativeQuery(sql);
		if(cardDto.getCardId()<=0){
		query.setParameter("lesson_id", cardDto.getLessonId());
		query.setParameter("question", cardDto.getQuestion());
		query.setParameter("answer", cardDto.getAnswer());
		query.setParameter("audio", cardDto.getAudio());
		query.setParameter("image", cardDto.getImage());
		query.setParameter("disp_order", cardDto.getDispOder());
		} else {
			query.setParameter("lesson_id", cardDto.getLessonId());
			query.setParameter("question", cardDto.getQuestion());
			query.setParameter("answer", cardDto.getAnswer());
			query.setParameter("audio", cardDto.getAudio());
			query.setParameter("image", cardDto.getImage());
			query.setParameter("disp_order", cardDto.getDispOder());
			query.setParameter("card_id", cardDto.getCardId());
		}
		return query.executeUpdate() > 0;
	}

	@Override
	public boolean deleteCard(CardDto cardDto) {
		String sql = AppConfig.SQL_DELETE_CARD;
		Query query = entityManager.createNativeQuery(sql);
		query.setParameter("card_id", cardDto.getCardId());
		return query.executeUpdate() > 0;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Card> findCardByLessonID(long lessonID) {
		Query query = entityManager.createNativeQuery(AppConfig.SQL_GET_CARD_BY_LESSON_ID);
		query.setParameter("lesson_id", lessonID);
		return query.getResultList();
	}


}
