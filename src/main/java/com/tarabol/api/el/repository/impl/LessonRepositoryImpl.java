package com.tarabol.api.el.repository.impl;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.tarabol.api.el.configuration.AppConfig;
import com.tarabol.api.el.repository.LessonRepositoryCustom;
import com.tarabol.dto.LessonDto;
import com.tarabol.model.Lesson;

@Repository
public class LessonRepositoryImpl implements LessonRepositoryCustom {

	@PersistenceContext
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	@Override
	public List<Lesson> findLessonByID(long lessonID) {
		Query query = entityManager.createNativeQuery(AppConfig.SQL_GET_LESSON_BY_ID);
		query.setParameter("lesson_id", lessonID);
		return query.getResultList();
	}

	@Override
	public boolean saveLesson(LessonDto lessonDto) {
		String sql = lessonDto.getLessonId() <= 0 ? AppConfig.SQL_ADD_LESSON : AppConfig.SQL_UPDATE_LESSON;
		Query query = entityManager.createNativeQuery(sql);
		if (lessonDto.getLessonId() <= 0) {
			query.setParameter("name", lessonDto.getName());
			query.setParameter("deck_id", lessonDto.getDeckId());
			query.setParameter("disp_order", lessonDto.getDispOrder());
		} else {
			query.setParameter("name", lessonDto.getName());
			query.setParameter("deck_id", lessonDto.getDeckId());
			query.setParameter("disp_order", lessonDto.getDispOrder());
			query.setParameter("disp_order", lessonDto.getDispOrder());
			query.setParameter("lesson_id", lessonDto.getDispOrder());

		}
		boolean rs = query.executeUpdate() > 0;
		if (rs) {
			BigInteger lastId = (BigInteger) ((Session) entityManager.getDelegate()).createSQLQuery("SELECT LAST_INSERT_ID()").uniqueResult();
			lessonDto.setLessonId(lastId.intValue());
		}
		return rs;
	}

	@Override
	public boolean deleteLesson(LessonDto lessonDto) {
		Query query = entityManager.createNativeQuery(AppConfig.SQL_DELETE_LESSON);
		query.setParameter("lesson_id", lessonDto);
		return query.executeUpdate() > 0;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Lesson> findLessonByDeckID(long deckID) {
		Query query = entityManager.createNativeQuery(AppConfig.SQL_GET_LESSON_BY_DECK_ID);
		query.setParameter("deck_id", deckID);
		List<Object[]> objects = query.getResultList();
		List<Lesson> rs = new ArrayList<Lesson>();
		for (Object[] cell : objects) {
			Lesson l = new Lesson();
			l.setDeckId(((BigInteger) cell[0]).longValue());
			l.setName((String) cell[1]);
			l.setDeckId(((BigInteger) cell[2]).longValue());
			l.setDispOrder(((BigInteger) cell[3]).longValue());
			rs.add(l);
		}
		return rs;
	}

}
