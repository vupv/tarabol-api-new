package com.tarabol.api.el.repository.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.tarabol.api.el.configuration.AppConfig;
import com.tarabol.api.el.repository.CategoryRepositoryCustom;
import com.tarabol.dto.CategoryDto;
import com.tarabol.model.Category;

@Repository
public class CategoryRepositoryImpl implements CategoryRepositoryCustom {
	
	@PersistenceContext
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	@Override
	public List<Category> findCategory() {
		
		Query query = entityManager.createNativeQuery(AppConfig.SQL_GET_CATEGORY, Category.class);
		return query.getResultList();
	}

	@Override
	public boolean addCategory(CategoryDto categoryDto) {
		String sql = AppConfig.SQL_ADD_CATEGORY;
		Query query = entityManager.createNativeQuery(sql);
//		query.setParameter("categories_id", categoryDto.getCategoriesId());
		query.setParameter("name", categoryDto.getName());
		query.setParameter("disp_order", categoryDto.getDispOrder());
		return query.executeUpdate() > 0;
	}

	@Override
	public boolean deleteCategory(CategoryDto categoryDto){
		Query query = entityManager.createNativeQuery(AppConfig.SQL_DELETE_CATEGORY);
		query.setParameter("categories_id", categoryDto.getCategoriesId());
		return query.executeUpdate() > 0;
	}

	@Override
	public boolean editCategory(CategoryDto categoryDto) {
		Query query = entityManager.createNativeQuery(AppConfig.SQL_UPDATE_CATEGORY);
		query.setParameter("name", categoryDto.getName());
		query.setParameter("disp_order", categoryDto.getDispOrder());
		query.setParameter("categories_id", categoryDto.getCategoriesId());
		return query.executeUpdate() > 0;
	}

}
