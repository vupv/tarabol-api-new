package com.tarabol.api.el.dto;

import java.io.Serializable;
import java.util.Date;

import com.tarabol.request.DeckRequest;

public class DeckDto implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long deckId;
	private long categoriesId;
	private Date createTime;
	private String description;
	private long evaluate;
	private String icon;
	private String name;
	private long price;
	private long status;
	private String tags;
	private long totalDownload;
	private Date updateTime;
	private long userId;
	private int first;
	private int max;

	public DeckDto(long deckId, long categoriesId, Date createTime, String description, long evaluate, String icon, String name, long price, long status, String tags, long totalDownload,
			Date updateTime, long userId) {
		super();

		this.deckId = deckId;
		this.categoriesId = categoriesId;
		this.createTime = createTime;
		this.description = description;
		this.evaluate = evaluate;
		this.icon = icon;
		this.name = name;
		this.price = price;
		this.status = status;
		this.tags = tags;
		this.totalDownload = totalDownload;
		this.updateTime = updateTime;
		this.userId = userId;
	}

	public DeckDto() {
		super();
	}

	/**
	 * @return the deckId
	 */
	public long getDeckId() {
		return deckId;
	}

	/**
	 * @param deckId
	 *            the deckId to set
	 */
	public void setDeckId(long deckId) {
		this.deckId = deckId;
	}

	/**
	 * @return the categoriesId
	 */
	public long getCategoriesId() {
		return categoriesId;
	}

	/**
	 * @param categoriesId
	 *            the categoriesId to set
	 */
	public void setCategoriesId(long categoriesId) {
		this.categoriesId = categoriesId;
	}

	/**
	 * @return the createTime
	 */
	public Date getCreateTime() {
		return createTime;
	}

	/**
	 * @param createTime
	 *            the createTime to set
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the evaluate
	 */
	public long getEvaluate() {
		return evaluate;
	}

	/**
	 * @param evaluate
	 *            the evaluate to set
	 */
	public void setEvaluate(long evaluate) {
		this.evaluate = evaluate;
	}

	/**
	 * @return the icon
	 */
	public String getIcon() {
		return icon;
	}

	/**
	 * @param icon
	 *            the icon to set
	 */
	public void setIcon(String icon) {
		this.icon = icon;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the price
	 */
	public long getPrice() {
		return price;
	}

	/**
	 * @param price
	 *            the price to set
	 */
	public void setPrice(long price) {
		this.price = price;
	}

	/**
	 * @return the status
	 */
	public long getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(long status) {
		this.status = status;
	}

	/**
	 * @return the tags
	 */
	public String getTags() {
		return tags;
	}

	/**
	 * @param tags
	 *            the tags to set
	 */
	public void setTags(String tags) {
		this.tags = tags;
	}

	/**
	 * @return the totalDownload
	 */
	public long getTotalDownload() {
		return totalDownload;
	}

	/**
	 * @param totalDownload
	 *            the totalDownload to set
	 */
	public void setTotalDownload(long totalDownload) {
		this.totalDownload = totalDownload;
	}

	/**
	 * @return the updateTime
	 */
	public Date getUpdateTime() {
		return updateTime;
	}

	/**
	 * @param updateTime
	 *            the updateTime to set
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * @return the userId
	 */
	public long getUserId() {
		return userId;
	}

	/**
	 * @param userId
	 *            the userId to set
	 */
	public void setUserId(long userId) {
		this.userId = userId;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the first
	 */
	public int getFirst() {
		return first;
	}

	/**
	 * @param first
	 *            the first to set
	 */
	public void setFirst(int first) {
		this.first = first;
	}

	/**
	 * @return the max
	 */
	public int getMax() {
		return max;
	}

	/**
	 * @param max
	 *            the max to set
	 */
	public void setMax(int max) {
		this.max = max;
	}

	/**
	 * convert deck request to dto
	 * 
	 * @param deck
	 * @return
	 */
	public static DeckDto convertDeckToDTO(DeckRequest deck) {
		DeckDto dto = new DeckDto();
		dto.categoriesId = deck.getCategoryId();
		dto.description = deck.getDescription();
		dto.name = deck.getName();
		dto.tags = deck.getTags();
		dto.price = (long) deck.getPrice();
		return dto;
	}

}
