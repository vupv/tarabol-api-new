package com.tarabol.api.el.dto;

import java.io.Serializable;
import java.util.Date;

public class DeckUserDto implements Serializable{
	private static final long serialVersionUID = 1L;
	private long deckUserId;
	private Date createTime;
	private long deckId;
	private long status;
	private Date updateTime;
	private long userId;
	
	public DeckUserDto(long deckUserId, Date createTime, long deckId,
			long status, Date updateTime, long userId) {
		super();
		this.deckUserId = deckUserId;
		this.createTime = createTime;
		this.deckId = deckId;
		this.status = status;
		this.updateTime = updateTime;
		this.userId = userId;
	}
	public DeckUserDto() {
		super();
	}
	/**
	 * @return the deckUserId
	 */
	public long getDeckUserId() {
		return deckUserId;
	}
	/**
	 * @param deckUserId the deckUserId to set
	 */
	public void setDeckUserId(long deckUserId) {
		this.deckUserId = deckUserId;
	}
	/**
	 * @return the createTime
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * @param createTime the createTime to set
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * @return the deckId
	 */
	public long getDeckId() {
		return deckId;
	}
	/**
	 * @param deckId the deckId to set
	 */
	public void setDeckId(long deckId) {
		this.deckId = deckId;
	}
	/**
	 * @return the status
	 */
	public long getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(long status) {
		this.status = status;
	}
	/**
	 * @return the updateTime
	 */
	public Date getUpdateTime() {
		return updateTime;
	}
	/**
	 * @param updateTime the updateTime to set
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	/**
	 * @return the userId
	 */
	public long getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(long userId) {
		this.userId = userId;
	}
	
	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
