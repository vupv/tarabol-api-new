package com.tarabol.api.el.dto;

import java.io.Serializable;
import java.util.Date;

public class CardDto implements Serializable{
	private static final long serialVersionUID = 1L;
	private long cardId;
	private String answer;
	private String audio;
	private Date createTime;
	private String image;
	private long lessonId;
	private String question;
	private long status;
	private Date updateTime;
	private long dispOrder;
	
	public CardDto(long cardId, String answer, String audio, Date createTime,
			String image, long lessonId, String question, long status,
			Date updateTime, long dispOrder) {
		super();
		this.cardId = cardId;
		this.answer = answer;
		this.audio = audio;
		this.createTime = createTime;
		this.image = image;
		this.lessonId = lessonId;
		this.question = question;
		this.status = status;
		this.updateTime = updateTime;
		this.dispOrder = dispOrder;
	}
	public CardDto() {
		super();
	}
	/**
	 * @return the cardId
	 */
	public long getCardId() {
		return cardId;
	}
	/**
	 * @param cardId the cardId to set
	 */
	public void setCardId(long cardId) {
		this.cardId = cardId;
	}
	/**
	 * @return the answer
	 */
	public String getAnswer() {
		return answer;
	}
	/**
	 * @param answer the answer to set
	 */
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	/**
	 * @return the audio
	 */
	public String getAudio() {
		return audio;
	}
	/**
	 * @param audio the audio to set
	 */
	public void setAudio(String audio) {
		this.audio = audio;
	}
	/**
	 * @return the createTime
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * @param createTime the createTime to set
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * @return the image
	 */
	public String getImage() {
		return image;
	}
	/**
	 * @param image the image to set
	 */
	public void setImage(String image) {
		this.image = image;
	}
	/**
	 * @return the lessonId
	 */
	public long getLessonId() {
		return lessonId;
	}
	/**
	 * @param lessonId the lessonId to set
	 */
	public void setLessonId(long lessonId) {
		this.lessonId = lessonId;
	}
	/**
	 * @return the question
	 */
	public String getQuestion() {
		return question;
	}
	/**
	 * @param question the question to set
	 */
	public void setQuestion(String question) {
		this.question = question;
	}
	/**
	 * @return the status
	 */
	public long getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(long status) {
		this.status = status;
	}
	/**
	 * @return the updateTime
	 */
	public Date getUpdateTime() {
		return updateTime;
	}
	/**
	 * @param updateTime the updateTime to set
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	/**
	 * @return the option
	 */
	public long getDispOder() {
		return dispOrder;
	}
	/**
	 * @param dispOrder the option to set
	 */
	public void setDispOder(long dispOrder) {
		this.dispOrder = dispOrder;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
