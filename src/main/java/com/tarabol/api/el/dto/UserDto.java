package com.tarabol.api.el.dto;

import java.io.Serializable;
import java.util.Date;

public class UserDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long userId;
	private String avatar;
	private String bio;
	private Date createTime;
	private String email;
	private long learnConfigId;
	private String name;
	private String password;
	private long status;
	private Date updateTime;
	private String username;
	private int first;
	private int max;
	
	public UserDto(long userId, String avatar, String bio, Date createTime,
			String email, long learnConfigId, String name, String password,
			long status, Date updateTime, String username) {
		super();
		this.userId = userId;
		this.avatar = avatar;
		this.bio = bio;
		this.createTime = createTime;
		this.email = email;
		this.learnConfigId = learnConfigId;
		this.name = name;
		this.password = password;
		this.status = status;
		this.updateTime = updateTime;
		this.username = username;
	}
	
	public UserDto() {
		super();
	}
	/**
	 * @return the userId
	 */
	public long getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(long userId) {
		this.userId = userId;
	}
	/**
	 * @return the avatar
	 */
	public String getAvatar() {
		return avatar;
	}
	/**
	 * @param avatar the avatar to set
	 */
	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
	/**
	 * @return the bio
	 */
	public String getBio() {
		return bio;
	}
	/**
	 * @param bio the bio to set
	 */
	public void setBio(String bio) {
		this.bio = bio;
	}
	/**
	 * @return the createTime
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * @param createTime the createTime to set
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return the learnConfigId
	 */
	public long getLearnConfigId() {
		return learnConfigId;
	}
	/**
	 * @param learnConfigId the learnConfigId to set
	 */
	public void setLearnConfigId(long learnConfigId) {
		this.learnConfigId = learnConfigId;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * @return the status
	 */
	public long getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(long status) {
		this.status = status;
	}
	/**
	 * @return the updateTime
	 */
	public Date getUpdateTime() {
		return updateTime;
	}
	/**
	 * @param updateTime the updateTime to set
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}
	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}
	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the first
	 */
	public int getFirst() {
		return first;
	}

	/**
	 * @param first the first to set
	 */
	public void setFirst(int first) {
		this.first = first;
	}

	/**
	 * @return the max
	 */
	public int getMax() {
		return max;
	}

	/**
	 * @param max the max to set
	 */
	public void setMax(int max) {
		this.max = max;
	}

	
}