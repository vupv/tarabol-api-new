package com.tarabol.api.el.dto;

import java.io.Serializable;

public class LessonDto implements Serializable{
	private static final long serialVersionUID = 1L;
	private long lessonId;
	private long deckId;
	private long dispOrder;
	private String name;
	
	public LessonDto(long lessonId, long deckId, long dispOrder, String name) {
		super();
		this.lessonId = lessonId;
		this.deckId = deckId;
		this.dispOrder = dispOrder;
		this.name = name;
	}
	
	public LessonDto() {
		super();
	}
	/**
	 * @return the lessonId
	 */
	public long getLessonId() {
		return lessonId;
	}
	/**
	 * @param lessonId the lessonId to set
	 */
	public void setLessonId(long lessonId) {
		this.lessonId = lessonId;
	}
	/**
	 * @return the deckId
	 */
	public long getDeckId() {
		return deckId;
	}
	/**
	 * @param deckId the deckId to set
	 */
	public void setDeckId(long deckId) {
		this.deckId = deckId;
	}
	/**
	 * @return the dispOrder
	 */
	public long getDispOrder() {
		return dispOrder;
	}
	/**
	 * @param dispOrder the dispOrder to set
	 */
	public void setDispOrder(long dispOrder) {
		this.dispOrder = dispOrder;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
