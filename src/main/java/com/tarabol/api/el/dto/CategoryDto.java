package com.tarabol.api.el.dto;

import java.io.Serializable;

public class CategoryDto implements Serializable{
	private static final long serialVersionUID = 1L;
	private long categoriesId;
	private String name;
	private long dispOrder;
	
	public CategoryDto(long categoriesId, String name, long dispOrder) {
		super();
		this.categoriesId = categoriesId;
		this.name = name;
		this.dispOrder = dispOrder;
	}

	public CategoryDto() {
		super();
	}

	/**
	 * @return the categoriesId
	 */
	public long getCategoriesId() {
		return categoriesId;
	}

	/**
	 * @param categoriesId the categoriesId to set
	 */
	public void setCategoriesId(long categoriesId) {
		this.categoriesId = categoriesId;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the dispOrder
	 */
	public long getDispOrder() {
		return dispOrder;
	}

	/**
	 * @param dispOrder the dispOrder to set
	 */
	public void setDispOrder(long dispOrder) {
		this.dispOrder = dispOrder;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
