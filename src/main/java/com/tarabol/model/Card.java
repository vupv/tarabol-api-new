package com.tarabol.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the CARD database table.
 * 
 */
@Entity
@Table(name="CARD")
@NamedQuery(name="Card.findAll", query="SELECT c FROM Card c")
public class Card implements Serializable {
	private static final long serialVersionUID = 1L;
	private long cardId;
	private String answer;
	private String audio;
	private Date createTime;
	private String image;
	private long lessonId;
	private String question;
	private long status;
	private Date updateTime;
	private long dispOder;

	public Card() {
	}


	@Id
	@Column(name="CARD_ID", unique=true, nullable=false)
	public long getCardId() {
		return this.cardId;
	}

	public void setCardId(long cardId) {
		this.cardId = cardId;
	}

	@Column (name="ANSWER", length=1000)
	public String getAnswer() {
		return this.answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	@Column (name="AUDIO", length=500)
	public String getAudio() {
		return this.audio;
	}

	public void setAudio(String audio) {
		this.audio = audio;
	}


	@Temporal(TemporalType.DATE)
	@Column(name="CREATE_TIME")
	public Date getCreateTime() {
		return this.createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	@Column (name="IMAGE", length=500)
	public String getImage() {
		return this.image;
	}

	public void setImage(String image) {
		this.image = image;
	}


	@Column(name="LESSON_ID")
	public long getLessonId() {
		return this.lessonId;
	}

	public void setLessonId(long lessonId) {
		this.lessonId = lessonId;
	}

	@Column (name="QUESTION", length=1000)
	public String getQuestion() {
		return this.question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	@Column (name="STATUS")
	public long getStatus() {
		return this.status;
	}

	public void setStatus(long status) {
		this.status = status;
	}


	@Temporal(TemporalType.DATE)
	@Column(name="UPDATE_TIME")
	public Date getUpdateTime() {
		return this.updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	@Column(name="DISP_ORDER")
	public long getDispOder() {
		return dispOder;
	}

	public void setDispOder(long dispOder) {
		this.dispOder = dispOder;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}